import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { CardSearchResponse } from 'src/app/api-contracts/results/card-search-response';
import { KeywordService } from 'src/app/services/keyword.service';
import { Card } from '../../models/card';
import { CardFilter } from '../../models/card-filter';
import { CardService } from '../../services/card.service';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent implements OnInit {
  keywords: string[] = [];
  searchResult$: Observable<CardSearchResponse> | undefined;

  searchForm!: FormGroup;
  keywordsControls!: FormArray;
  page: number = 1;

  constructor(
    private cardService: CardService,
    private keywordService: KeywordService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.keywordsControls = this.formBuilder.array([]);

    this.searchForm = this.formBuilder.group({
      includeUnverified: [false],
      title: [''],
      author: [''],
      after: [null],
      before: [null],
      keywords: this.keywordsControls
    });

    this.setFormFromQueryParams();

    const paramKeywords = this.route.snapshot.queryParamMap.getAll('keyword');

    this.getKeywords()
      .subscribe(keywords => {
        keywords.forEach(keyword => {
          const control = this.formBuilder.control(paramKeywords.includes(keyword));
          this.keywordsControls.push(control);
          this.keywords.push(keyword);
        });
        
        this.getCards();
      });
  }

  getKeywords(): Observable<string[]> {
    return this.keywordService.getNames();
  }

  get includeUnverified() {
    return this.searchForm.get('includeUnverified')!.value;
  }

  get title() {
    return this.searchForm.get('title')!.value;
  }

  get author() {
    return this.searchForm.get('author')!.value;
  }

  get after() {
    let afterValue = this.searchForm.get('after')!.value;
    return afterValue ? new Date(afterValue) : undefined;
  }

  get before() {
    let beforeValue = this.searchForm.get('before')!.value;
    return beforeValue ? new Date(beforeValue) : undefined;
  }

  get selectedKeywords() {
    return this.keywordsControls.controls
      .map((c, i) => {
        return { checked: c.value, keyword: this.keywords[i] }
      })
      .filter(x => x.checked)
      .map(x => x.keyword);
  }

  getCards() {
    const filter = new CardFilter();
    filter.title = this.title;
    filter.author = this.author;
    filter.includeUnverified = this.includeUnverified;
    filter.after = this.after;
    filter.before = this.before;
    filter.keywords = this.selectedKeywords;

    this.searchResult$ = this.cardService.getByFilter(filter, this.page, 20);
  }

  onFilter() {
    this.page = 1;
    this.setParamsFromForm();
    this.getCards();
  }

  onClearFilters() {
    this.page = 1;
    
    this.searchForm.patchValue({
      includeUnverified: false,
      title: null,
      author: null,
      after: null,
      before: null,
      keywords: this.keywords.map(x => false)
    });

    this.clearParams();
    this.getCards();
  }

  onChangePageClick(page: number) {
    this.page = page;

    const params = this.createParams();
    this.router.navigate([], { relativeTo: this.route, queryParams: params });
    window.scroll({ left: 0, top: 0, behavior: "smooth" });
    this.getCards();
  }

  createParams(): Params {
    let params: Params = {};
    
    if(this.includeUnverified) {
      params['includeUnverified'] = this.includeUnverified;
    }

    if (this.title) {
      params['title'] = this.title;
    }
    
    if (this.author) {
      params['author'] = this.author;
    }
    
    if (this.after) {
      params['after'] = this.after.toUTCString();
    }
    
    if (this.before) {
      params['before'] = this.before.toUTCString();
    }

    params['keyword'] = this.selectedKeywords;
    
    if (this.page > 1) {
      params['page'] = this.page;
    }

    return params;
  }

  setFormFromQueryParams() {
    const params = this.route.snapshot.queryParamMap;

    const values = {
      includeUnverified: params.get('includeUnverified'),
      title: params.get('title'),
      author: params.get('author'),
      after: this.toDateInputString(params.get('after')),
      before: this.toDateInputString(params.get('before'))
    }

    this.searchForm.patchValue(values);

    this.page = parseInt(params.get('page') ?? '1');
  }

  setParamsFromForm() {
    const params = this.createParams();
    this.router.navigate([], { relativeTo: this.route, queryParams: params });
  }

  clearParams() {
    this.router.navigate([], { relativeTo: this.route });
  }

  toDateInputString(value: string | Date | number | null): string {
    if (!value) {
      return '';
    }

    const date = new Date(value);

    if (isNaN(date.getTime())) {
      return '';
    }

    return formatDate(date, 'yyyy-MM-dd', 'en');
  }
}
