﻿namespace CardFile.Domain.Filters
{
    /// <summary>
    /// Represents criteria by which index cards can be filtered.
    /// </summary>
    public class CardSearchFilter
    {
        public string Title { get; set; }
        public string AuthorId { get; set; }
        public string Author { get; set; }
        public bool IncludeUnverified { get; set; }
        public DateTime? After { get; set; }
        public DateTime? Before { get; set; }
        public IEnumerable<string> Keywords { get; set; }
    }
}
