﻿using CardFile.Application.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Validators.ValidationRules
{
    /// <summary>
    /// Contains extension methods that define validation rules for index card properties.
    /// </summary>
    public static class CardValidationRules
    {
        /// <summary>
        /// Defines validation rules for card title.
        /// </summary>
        /// <typeparam name="T">type of object being validated</typeparam>
        /// <param name="ruleBuilder">the rule builder on which the validator should be defined</param>
        /// <returns>A reference to this instance after the operation has completed.</returns>
        public static IRuleBuilderOptions<T, string> ValidCardTitle<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .NotEmpty()
                .MaximumLength(200);
        }

        /// <summary>
        /// Defines validation rules for card content.
        /// </summary>
        /// <typeparam name="T">type of object being validated</typeparam>
        /// <param name="ruleBuilder">the rule builder on which the validator should be defined</param>
        /// <returns>A reference to this instance after the operation has completed.</returns>
        public static IRuleBuilderOptions<T, string> ValidCardContent<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder.NotNull();
        }

        /// <summary>
        /// Defines validation rules for card keywords.
        /// </summary>
        /// <typeparam name="T">type of object being validated</typeparam>
        /// <param name="ruleBuilder">the rule builder on which the validator should be defined</param>
        /// <returns>A reference to this instance after the operation has completed.</returns>
        public static IRuleBuilderOptions<T, IEnumerable<string>> ValidCardKeywords<T>(this IRuleBuilder<T, IEnumerable<string>> ruleBuilder)
        {
            return ruleBuilder
                .ForEach(rule => rule.ValidKeywordName());
        }

        /// <summary>
        /// Defines validation rules for card keywords.
        /// </summary>
        /// <typeparam name="T">type of object being validated</typeparam>
        /// <param name="ruleBuilder">the rule builder on which the validator should be defined</param>
        /// <returns>A reference to this instance after the operation has completed.</returns>
        public static IRuleBuilderOptions<T, IEnumerable<KeywordModel>> ValidCardKeywords<T>(this IRuleBuilder<T, IEnumerable<KeywordModel>> ruleBuilder)
        {
            return ruleBuilder
                .ForEach(rule => rule.SetValidator(new KeywordValidator()));
        }
    }
}
