﻿namespace CardFile.Domain.Entities
{
    /// <summary>
    /// Represents a keyword used to categorize index cards.
    /// </summary>
    public class Keyword
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Card> Cards { get; set; }
    }
}
