﻿using CardFile.Application.Interfaces;

namespace CardFile.Application.Services
{
    /// <summary>
    /// Am <see cref="IDateTimeProvider"/> implementation.
    /// </summary>
    public class DateTimeProvider : IDateTimeProvider
    {
        /// <inheritdoc/>
        public DateTime UtcNow => DateTime.UtcNow;
    }
}
