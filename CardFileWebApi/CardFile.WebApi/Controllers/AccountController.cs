﻿using AutoMapper;
using CardFile.Application.Exceptions;
using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using CardFile.WebApi.Models.Requests;
using CardFile.WebApi.Models.Responses;
using CardFile.WebApi.Validation;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CardFile.WebApi.Controllers
{
    /// <summary>
    /// Provides endpoints for authentication, registration and updating user accounts.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly AbstractValidator<UserSearchQueryParameters> _searchQueryValidator;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountController"/> class.
        /// </summary>
        /// <param name="accountService">The account service</param>
        /// <param name="searchQueryValidator">User search query parameters validator</param>
        /// <param name="mapper">Mapper</param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="accountService"/> is <see langword="null"/>.
        /// </exception>
        public AccountController(
            IAccountService accountService,
            AbstractValidator<UserSearchQueryParameters> searchQueryValidator,
            IMapper mapper)
        {
            _accountService = accountService ?? throw new ArgumentNullException(nameof(accountService));
            _searchQueryValidator = searchQueryValidator ?? throw new ArgumentNullException(nameof(searchQueryValidator));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Registers new user.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Response with JWT or errors</returns>
        /// <response code="200">Returns success response containing JWT</response>
        /// <response code="400">Returns error response if registration failed</response>
        [HttpPost("register")]
        [SwaggerResponse(((int)HttpStatusCode.OK), type: typeof(AuthSuccessResponse))]
        [SwaggerResponse(((int)HttpStatusCode.BadRequest), type: typeof(AuthFailedResponse))]
        public async Task<IActionResult> Register([FromBody] RegistrationRequest request)
        {
            try
            {
                var token = await _accountService.RegisterAsync(request.UserName, request.Password);

                return Ok(new AuthSuccessResponse(token));
            }
            catch (AuthenticationException e)
            {
                return BadRequest(new AuthFailedResponse(e.Errors));
            }
        }

        /// <summary>
        /// Authenticates user and returns JWT.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Response with JWT or errors</returns>
        /// <response code="200">Success response containing JWT</response>
        /// <response code="400">Error response if registration failed</response>
        [HttpPost("login")]
        [SwaggerResponse(((int)HttpStatusCode.OK), type: typeof(AuthSuccessResponse))]
        [SwaggerResponse(((int)HttpStatusCode.BadRequest), type: typeof(AuthFailedResponse))]
        public async Task<IActionResult> Login([FromBody] LoginRequest request)
        {
            try
            {
                var token = await _accountService.LoginAsync(request.UserName, request.Password);

                return Ok(new AuthSuccessResponse(token));
            }
            catch (AuthenticationException e)
            {
                return BadRequest(new AuthFailedResponse(e.Errors));
            }
        }

        /// <summary>
        /// Changes a user's password after confirming the specified currentPassword is correct.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Empty response or errors</returns>
        /// <response code="204">Empty response indicating that password changed successfully</response>
        /// <response code="400">Error response if password change failed</response>
        [HttpPost("change-password")]
        [SwaggerResponse(((int)HttpStatusCode.NoContent))]
        [SwaggerResponse(((int)HttpStatusCode.BadRequest), type: typeof(AuthFailedResponse))]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordRequest request)
        {
            try
            {
                await _accountService.ChangePasswordAsync(request.UserName, request.CurrentPassword, request.NewPassword);

                return NoContent();
            }
            catch (AuthenticationException e)
            {
                return BadRequest(new AuthFailedResponse(e.Errors));
            }
        }

        /// <summary>
        /// Changes user's role.
        /// </summary>
        /// <param name="request"></param>
        /// <returns>Empty response or errors</returns>
        /// <response code="204">Empty response indicating that role was changed successfully</response>
        /// <response code="400">Error response if role change failed</response>
        [HttpPost("change-role")]
        [Authorize(Roles = UserRoles.Admin)]
        [SwaggerResponse((int)HttpStatusCode.NoContent)]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, type: typeof(AuthFailedResponse))]
        public async Task<IActionResult> ChangeRole([FromBody] ChangeRoleRequest request)
        {
            try
            {
                await _accountService.ChangeRoleAsync(request.UserId, request.Role);

                return NoContent();
            }
            catch (AuthenticationException e)
            {
                return BadRequest(new AuthFailedResponse(e.Errors));
            }
        }

        /// <summary>
        /// Retrieves users.
        /// </summary>
        /// <returns>
        /// Response with users.
        /// </returns>
        /// <response code="200">Response containing retrieved users</response>
        [HttpGet("users")]
        [SwaggerResponse((int)HttpStatusCode.OK, type: typeof(UserSearchResponse))]
        public async Task<ActionResult<UserSearchResponse>> GetUsers([FromQuery] UserSearchQueryParameters parameters)
        {
            _searchQueryValidator.ValidateAndThrow(parameters);

            IEnumerable<UserModel> users;
            int totalCount;

            if (parameters.IsFilterEmpty())
            {
                totalCount = await _accountService.GetUserCountAsync();
                users = await _accountService.GetUsersAsync(parameters.Limit, parameters.Offset);
            }
            else
            {
                var filter = _mapper.Map<UserSearchFilterModel>(parameters);
                totalCount = await _accountService.GetUserCountAsync(filter);
                users = await _accountService.GetUsersAsync(parameters.Limit, parameters.Offset, filter);
            }

            var response = new UserSearchResponse
            {
                Users = users,
                Limit = parameters.Limit,
                Offset = parameters.Offset,
                TotalCount = totalCount
            };

            return Ok(response);
        }

        /// <summary>
        /// Retrieves a user by id.
        /// </summary>
        /// <param name="id">The user id</param>
        /// <returns>Response with the keyword or NotFound.</returns>
        /// <response code="200">Response containing the user</response>
        /// <response code="404">User not found</response>
        [HttpGet("users/{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, type: typeof(UserModel))]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<UserModel>> GetUser(string id)
        {
            var user = await _accountService.GetUserByIdAsync(id);

            if (user is null)
            {
                return NotFound();
            }

            return Ok(user);
        }
    }
}
