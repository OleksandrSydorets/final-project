export interface Card {
    id: number;
    title: string;
    authorId: string;
    author: string;
    createdAt: string;
    verified: boolean;
    keywords: string[];
}