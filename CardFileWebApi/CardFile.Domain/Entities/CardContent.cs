﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Domain.Entities
{
    /// <summary>
    /// Represents an index cards content.
    /// </summary>
    public class CardContent
    {
        public int CardId { get; set; }
        public string Content { get; set; }
    }
}
