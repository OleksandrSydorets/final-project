export class ChangeRoleRequest {
    constructor (private userId: string, private role: string) {}
}