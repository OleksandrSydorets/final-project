﻿using CardFile.Domain.Entities;

namespace CardFile.Persistence.Entities
{
    /// <summary>
    /// An associative entity for many-to-many relationship between <see cref="CardFile.Domain.Entities.Card"/> and <see cref="CardFile.Domain.Entities.Keyword"/>.
    /// </summary>
    public class CardKeywords
    {
        public int CardId { get; set; }
        public int KeywordId { get; set; }

        public Card Card { get; set; }
        public Keyword Keyword { get; set; }
    }
}
