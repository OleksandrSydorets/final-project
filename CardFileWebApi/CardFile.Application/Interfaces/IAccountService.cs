﻿using CardFile.Application.Exceptions;
using CardFile.Application.Models;

namespace CardFile.Application.Interfaces
{
    /// <summary>
    /// Defines functionality for registering new users, changing passwords and generating bearer tokens.
    /// </summary>
    public interface IAccountService
    {
        /// <summary>
        /// Creates a new user account.
        /// </summary>
        /// <param name="userName">New user's username</param>
        /// <param name="password">New user's password</param>
        /// <returns>
        ///     A task that represent the asynchronous operation. 
        ///     Task result contains JWT Bearer token.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="userName"/> or <paramref name="password"/> is null.</exception>
        /// <exception cref="AuthenticationException">Registration error.</exception>
        Task<string> RegisterAsync(string userName, string password);

        /// <summary>
        /// Creates JWT Bearer token.
        /// </summary>
        /// <param name="userName">Username</param>
        /// <param name="password">Password</param>
        /// <returns>
        ///     A task that represent the asynchronous operation. 
        ///     Task result contains JWT Bearer token.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="userName"/> or <paramref name="password"/> is null.</exception>
        /// <exception cref="AuthenticationException">Authentication error.</exception>
        Task<string> LoginAsync(string userName, string password);

        /// <summary>
        /// Changes user's password.
        /// </summary>
        /// <param name="userName">Username</param>
        /// <param name="currentPassword">Current password</param>
        /// <param name="newPassword">New Password</param>
        /// <returns>A task that represent the asynchronous operation.</returns>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="userName"/>, <paramref name="currentPassword"/> or <paramref name="newPassword"/> is null.
        /// </exception>
        /// <exception cref="AuthenticationException">Authentication error.</exception>
        Task ChangePasswordAsync(string userName, string currentPassword, string newPassword);

        /// <summary>
        /// Changes user's role.
        /// </summary>
        /// <param name="userId">The user id</param>
        /// <param name="role">User's new role</param>
        /// <returns>A task that represent the asynchronous operation.</returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="userId"/> or <paramref name="role"/> is <see langword="null"/>.
        /// </exception>
        /// <exception cref="AuthenticationException">Identity error.</exception>
        Task ChangeRoleAsync(string userId, string role);

        /// <summary>
        /// Retrieves users. If <paramref name="filter"/> is not <see langword="null"/> filters users.
        /// </summary>
        /// <param name="limit">Max amount of users to return</param>
        /// <param name="offset">Amount of users to skip</param>
        /// <param name="filter">The filter to use</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved users.
        /// </returns>
        Task<IEnumerable<UserModel>> GetUsersAsync(int limit = 10, int offset = 0, UserSearchFilterModel filter = null);

        /// <summary>
        /// Counts the number of users. If <paramref name="filter"/> is not <see langword="null"/>
        /// counts only filter users.
        /// </summary>
        /// <param name="filter">The filter</param>
        /// <returns>The number of users</returns>
        Task<int> GetUserCountAsync(UserSearchFilterModel filter = null);

        /// <summary>
        /// Retrieve user by id.
        /// </summary>
        /// <param name="id">The user id</param>
        /// <returns>A task that represent the asynchronous operation. 
        /// The task result contains retrieved user or <see langword="null"/>.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="id"/> is <see langword="null"/>.
        /// </exception>
        Task<UserModel> GetUserByIdAsync(string id);
    }
}
