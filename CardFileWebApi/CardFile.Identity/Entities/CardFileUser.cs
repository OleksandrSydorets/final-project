﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;

namespace CardFile.Identity.Entities
{
    /// <summary>
    /// Represent a user of this application in identity system.
    /// </summary>
    public class CardFileUser : IdentityUser
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CardFileUser"/>.
        /// </summary>
        public CardFileUser() : base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CardFileUser"/> and sets their UserName.
        /// </summary>
        /// <param name="userName">The user name</param>
        public CardFileUser(string userName) : base(userName) { }

        /// <summary>
        /// Roles that this users belongs to.
        /// </summary>
        public ICollection<CardFileRole> Roles { get; set; }
    }
}
