﻿namespace CardFile.WebApi.Models.Responses
{
    public class ValidationErrorResponse
    {
        public ValidationErrorResponse(Dictionary<string, IEnumerable<string>> errors)
        {
            Errors = errors;
        }

        public string Title { get; } = "One or more validation errors occurred.";

        public Dictionary<string, IEnumerable<string>> Errors { get; }
    }
}
