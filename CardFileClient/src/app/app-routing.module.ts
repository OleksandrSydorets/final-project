import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardComponent } from './components/card/card.component';
import { AuthGuard } from './guards/auth.guard';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { CardsComponent } from './components/cards/cards.component';
import { CreateCardComponent } from './components/create-card/create-card.component';
import { EditCardComponent } from './components/edit-card/edit-card.component';
import { ModeratorGuard } from './guards/moderator.guard';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { PageForbiddenComponent } from './components/page-forbidden/page-forbidden.component';
import { KeywordsComponent } from './components/keywords/keywords.component';
import { UsersComponent } from './components/users/users.component';
import { AdminGuard } from './guards/admin.guard';

const routes: Routes = [
  { path: "", redirectTo: "cards", pathMatch: "full" },
  { path: "cards", component: CardsComponent/*, canActivate: [AuthGuard]*/ },
  { path: "create-card", component: CreateCardComponent, canActivate: [AuthGuard] },
  { path: "cards/:id", component: CardComponent },
  { path: "edit-card/:id", component: EditCardComponent, canActivate: [ModeratorGuard] },
  { path: "keywords", component: KeywordsComponent, canActivate: [ModeratorGuard] },
  { path: "users", component: UsersComponent, canActivate: [AdminGuard] },
  { path: "login", component: LoginComponent },
  { path: "register", component: RegistrationComponent },
  { path: "forbidden", component: PageForbiddenComponent },
  { path: "**", component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
