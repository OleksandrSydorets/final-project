import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Params, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { UserSearchResponse } from 'src/app/api-contracts/results/user-search-response';
import { UserFilter } from 'src/app/models/user-filter';
import { UserRole } from 'src/app/models/user-role';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {
  roleOptions!: { value?: string, display: string}[];
  searchResult$: Observable<UserSearchResponse> | undefined;

  searchForm!:FormGroup;
  page: number = 1;
  routerEventsSubscription: Subscription;

  constructor(
    private userService: UserService, 
    private formBuilder: FormBuilder, 
    private route: ActivatedRoute, 
    private router: Router
  ) {
    // Maker component reloadable
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    this.routerEventsSubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    })
  }

  ngOnDestroy(): void {
    if (this.routerEventsSubscription) {
     this.routerEventsSubscription.unsubscribe();
    }
  }

  get userName(): string {
    return this.searchForm.get('userName')!.value;
  }

  get role(): string | undefined {
    return this.searchForm.get('role')!.value.value;
  }

  ngOnInit(): void {
    this.setRoleOptions();

    this.searchForm = this.formBuilder.group({
      userName: [null],
      role: [this.roleOptions[0]],
    });

    this.setFormFromQueryParams();
    this.getUsers();
  }

  getUsers() {
    const filter = new UserFilter();
    filter.userName = this.userName;
    filter.role = this.role;

    this.searchResult$ = this.userService.getByFilter(filter, this.page);
  }

  onFilter() {
    this.page = 1;
    this.setParamsFromForm();
    this.getUsers();
  }

  onClearFilters() {
    this.page = 1;

    this.searchForm.patchValue({
      userName: null,
      role: this.roleOptions[0],
    });

    this.clearParams();
    this.getUsers();
  }

  onChangePageClick(page: number) {
    this.page = page;
    this.setParamsFromForm();
    window.scroll({ left: 0, top: 0, behavior: "smooth" });
    this.getUsers();
  }

  setRoleOptions() {
    this.roleOptions = UserRole.Roles
    .map(r => ({ value: r, display: r}));
  
    this.roleOptions.unshift({ value: undefined, display: "Any"});
  }

  setParamsFromForm() {
    const params = this.createParams();
    this.router.navigate([], { relativeTo: this.route, queryParams: params });
  }

  createParams(): Params {
    let params: Params = {};

    if (this.userName) {
      params['userName'] = this.userName;
    }

    if (this.role) {
      params['role'] = this.role;
    }
    
    if (this.page > 1) {
      params['page'] = this.page;
    }

    return params;
  }

  clearParams() {
    this.router.navigate([], { relativeTo: this.route });
  }

  setFormFromQueryParams() {
    const params = this.route.snapshot.queryParamMap;
    const role = params.get('role');


    this.searchForm.patchValue({
      userName: params.get('userName'),
      role: this.roleOptions.find(o => o.value === role) ?? this.roleOptions[0],
    });

    this.page = parseInt(params.get('page') ?? '1');
  }
}
