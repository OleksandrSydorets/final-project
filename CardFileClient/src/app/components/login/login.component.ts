import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginResult } from '../../models/login-result';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  errors: string[] | null = null;

  loginForm!: FormGroup;

  constructor(private authService: AuthenticationService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required],
    })
  }

  onSubmit() {
    if (this.loginForm.invalid) {
      this.loginForm.markAllAsTouched();
      return;
    }

    this.loginForm.disable();
    this.errors = null;

    const userName: string = this.loginForm.get('userName')!.value;
    const password: string = this.loginForm.get('password')!.value;

    this.authService.login(userName, password)
      .subscribe(result => {
        if (result.successful) {
          this.router.navigate(['']);
        }
        else {
          this.errors = result.errors!;
        }
        
        this.loginForm.enable();
      });
  }

  controlValidCss(controlName: string) {
    const control = this.loginForm.get(controlName);

    if (!control) {
      return { };
    }

    return {
      'is-invalid': control.invalid && control.touched,
    }
  }
}
