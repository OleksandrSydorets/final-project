﻿using AutoMapper;
using CardFile.Application.Exceptions;
using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using CardFile.Application.Validators;
using CardFile.Domain;
using CardFile.Domain.Entities;
using CardFile.Domain.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Services
{
    /// <summary>
    /// An <see cref="IKeywordService"/> implementation.
    /// </summary>
    public class KeywordService : IKeywordService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IKeywordRepository _keywordRepository;
        private readonly IMapper _mapper;
        private readonly AbstractValidator<CreateKeywordModel> _createKeywordValidator;
        private readonly AbstractValidator<KeywordModel> _keywordValidator;

        /// <summary>
        /// Initializes a new instance of the <see cref="KeywordService"/> class.
        /// </summary>
        /// <param name="unitOfWork">unit of work</param>
        /// <param name="mapper">mapper</param>
        /// <param name="createKeywordValidator">validator for <see cref="CreateKeywordModel"/></param>
        /// <param name="keywordValidator">validator for <see cref="KeywordModel"/></param>
        /// <exception cref="ArgumentNullException">If any of the dependencies are <see langword="null"/></exception>
        public KeywordService(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            AbstractValidator<CreateKeywordModel> createKeywordValidator,
            AbstractValidator<KeywordModel> keywordValidator)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _createKeywordValidator = createKeywordValidator ?? throw new ArgumentNullException(nameof(createKeywordValidator));
            _keywordValidator = keywordValidator ?? throw new ArgumentNullException(nameof(keywordValidator));

            _keywordRepository = _unitOfWork.KeywordRepository;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<KeywordModel>> GetAllAsync()
        {
            var keywords = await _keywordRepository.GetAllAsync();

            return _mapper.Map<IEnumerable<KeywordModel>>(keywords);
        }

        /// <inheritdoc/>
        public async Task<KeywordModel> GetByIdAsync(int id)
        {
            var keyword = await _keywordRepository.GetByIdAsync(id);

            return _mapper.Map<KeywordModel>(keyword);
        }

        /// <inheritdoc/>
        public async Task<KeywordModel> AddAsync(CreateKeywordModel keyword)
        {
            ArgumentNullException.ThrowIfNull(keyword);
            _createKeywordValidator.ValidateAndThrow(keyword);

            var existing = await _keywordRepository.GetByNameAsync(keyword.Name);

            if (existing is not null)
                throw new KeywordAlreadyExistsException();

            var entity = new Keyword { Name = keyword.Name };

            await _keywordRepository.AddAsync(entity);
            await _unitOfWork.SaveAsync();

            return _mapper.Map<KeywordModel>(entity);
        }

        /// <inheritdoc/>
        public async Task UpdateAsync(KeywordModel keyword)
        {
            ArgumentNullException.ThrowIfNull(keyword);
            _keywordValidator.ValidateAndThrow(keyword);

            var entity = await _keywordRepository.GetByIdAsync(keyword.Id);

            if (entity is null)
                throw new EntityNotFoundException();

            var existing = await _keywordRepository.GetByNameAsync(keyword.Name);

            if (existing is not null && existing.Id != keyword.Id)
                throw new KeywordAlreadyExistsException();

            entity.Name = keyword.Name;
            _keywordRepository.Update(entity);
            await _unitOfWork.SaveAsync();
        }

        /// <inheritdoc/>
        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _keywordRepository.GetByIdAsync(id);

            if (entity is null)
                throw new EntityNotFoundException();

            _keywordRepository.Delete(entity);
            await _unitOfWork.SaveAsync();
        }
    }
}
