﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CardFile.Persistence.Migrations
{
    public partial class AddIndexForCardCreatedAt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Cards_CreatedAt",
                table: "Cards",
                column: "CreatedAt");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Cards_CreatedAt",
                table: "Cards");
        }
    }
}
