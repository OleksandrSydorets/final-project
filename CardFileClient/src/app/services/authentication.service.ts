import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, of, BehaviorSubject } from 'rxjs';
import { LoginRequest } from '../api-contracts/requests/login-request';
import { RegistrationRequest } from '../api-contracts/requests/registration-request';
import { AuthFailedResponse, isAuthFailedResponse } from '../api-contracts/results/auth-failed-response';
import { AuthSuccessResponse } from '../api-contracts/results/auth-success-response';
import { ApiPaths } from '../api-paths';
import { LoginResult } from '../models/login-result';
import { RegistrationResult } from '../models/registration-result';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private _token: string | null = null;
  private _tokenExpiration: Date | null = null;

  private _authenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  private _role: string | null = null;

  constructor(private httpClient: HttpClient) { 
    this.LoadTokenFromLocalStorage();
  }

  login(username: string, password: string): Observable<LoginResult> {
    let request = new LoginRequest(username, password);
    return this.httpClient.post<AuthSuccessResponse>(ApiPaths.Account.Login, request, { observe: 'response' })
      .pipe(
        map(resp => {
          this.token = resp.body!.token;
          return new LoginResult(true);
        }),
        catchError((err: HttpErrorResponse) => {
          if (err.status === 400 && isAuthFailedResponse(err.error)) {
            return of (new LoginResult(false, err.error.errors));
          }

          if (err.status === 0) {
            return of(new LoginResult(false, ['Connection error']));
          }

          return of(new LoginResult(false, ['Unknownw Error'] ));
        })
      );
  }

  logout() {
    this.token = null;
  }

  register(username: string, password: string): Observable<RegistrationResult> {
    let request = new RegistrationRequest(username, password);
    return this.httpClient.post<AuthSuccessResponse>(ApiPaths.Account.Register, request, { observe: 'response' })
      .pipe(
        map(resp => {
          this.token = resp.body!.token;
          return new RegistrationResult(true);
        }),
        catchError((err: HttpErrorResponse) => {
          if (err.status === 400 && isAuthFailedResponse(err.error)) {
            return of(new RegistrationResult(false, err.error.errors));
          }

          if (err.status === 0) {
            return of(new RegistrationResult(false, ['Connection error']));
          }

          return of(new RegistrationResult(false, ['Unknownw Error'] ));
        })
      );
  }

  get token(): string | null {
    if (this._token && this.isTokenExpired) {
      this.logout();
    }

    return this._token;
  }

  private set token(token: string | null) {
    let expiration: Date | null = token ? this.parseExpiration(token) : null;

    if (expiration && expiration < new Date()) {
      expiration = null;
      token = null;
    }
    
    this._token = token;
    this._tokenExpiration = expiration;
    this._role = token ? this.parseRole(token) : null;
    this.SaveTokenToLocalStorage();
    
    if (this._authenticated.value !== !!token) {
      this._authenticated.next(!!token);
    }
  }

  public get authenticated(): boolean {
    return this._authenticated.value;
  }

  public get role(): string | null {
    return this._role;
  }

  public get isAuthenticated(): Observable<boolean> {
    return this._authenticated.asObservable();
  }

  private get tokenExpiration(): Date | null {
    return this._tokenExpiration;
  }

  private get isTokenExpired(): boolean {
    return this._tokenExpiration ? this._tokenExpiration < new Date() : true;
  }

  private SaveTokenToLocalStorage() {
    const token = this.token;
    if (token === null) {
      localStorage.removeItem('token');
    } else if (localStorage.getItem('token') !== token){
      localStorage.setItem('token', token);
    }
  }

  private LoadTokenFromLocalStorage() {
    this.token = localStorage.getItem('token');
  }

  private parseExpiration(token: string): Date {
    const body = JSON.parse(atob(token.split('.')[1]));
    return new Date(body.exp * 1000);
  }

  private parseRole(token: string): string {
    return JSON.parse(atob(token.split('.')[1])).role;
  }
}
