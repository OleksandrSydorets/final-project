﻿using System.Collections.Generic;

namespace CardFile.WebApi.Models.Responses
{
    public class AuthFailedResponse
    {
        public AuthFailedResponse(IEnumerable<string> errors)
        {
            Errors = errors;
        }

        public IEnumerable<string> Errors { get; }
    }
}
