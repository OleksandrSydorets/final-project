import { HttpParams } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { CardSearchResponse } from 'src/app/api-contracts/results/card-search-response';

@Component({
  selector: 'app-card-search-result',
  templateUrl: './card-search-result.component.html',
  styleUrls: ['./card-search-result.component.css']
})
export class CardSearchResultComponent implements OnInit {
  @Input() searchResult: CardSearchResponse | null = null;
  @Output() changePageClick = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  onChangePageClick(page: number) {
    this.changePageClick.emit(page);
  }
}
