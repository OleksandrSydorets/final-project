export class CreateCardRequest {
    title: string;
    content: string;
    keywords: string[];

    constructor(title: string, content: string, keywords: string[]) {
        this.title = title;
        this.content = content;
        this.keywords = keywords;
    }
}