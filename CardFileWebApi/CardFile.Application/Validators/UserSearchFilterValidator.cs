﻿using CardFile.Application.Models;
using FluentValidation;

namespace CardFile.Application.Validators
{
    /// <summary>
    /// <see cref="FluentValidation"/> validator for <see cref="UserSearchFilterModel"/>.
    /// </summary>
    public class UserSearchFilterValidator : AbstractValidator<UserSearchFilterModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserSearchFilterValidator"/> class.
        /// </summary>
        public UserSearchFilterValidator()
        {
            RuleFor(m => m.Role)
                .Must(BeNullOrValidRole).WithMessage("{PropertyValue} is not a valid role.");
        }

        private static bool BeNullOrValidRole(string role)
        {
            if (string.IsNullOrEmpty(role))
            {
                return true;
            }

            return UserRoles.Roles.Contains(role);
        }
    }
}
