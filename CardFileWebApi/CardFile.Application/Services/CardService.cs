﻿using AutoMapper;
using CardFile.Application.Exceptions;
using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using CardFile.Application.RequiredInterfaces;
using CardFile.Application.Validators;
using CardFile.Domain;
using CardFile.Domain.Entities;
using CardFile.Domain.Filters;
using CardFile.Domain.Repositories;
using FluentValidation;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Services
{
    /// <summary>
    /// An <see cref="ICardService"/> implementation.
    /// </summary>
    public class CardService : ICardService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IAuthenticatedUserService _authUser;
        private readonly IDateTimeProvider _dateTime;
        private readonly AbstractValidator<CreateCardModel> _createCardValidator;
        private readonly AbstractValidator<CardSearchFilterModel> _filterValidator;
        private readonly AbstractValidator<UpdateCardModel> _updateCardValidator;
        private readonly ICardRepository _cardRepository;
        private readonly IKeywordRepository _keywordRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="CardService"/> class.
        /// </summary>
        /// <param name="unitOfWork">unit of work</param>
        /// <param name="mapper">mapper</param>
        /// <param name="authUser">authenticated user service</param>
        /// <param name="dateTime">datetime provider</param>
        /// <param name="createCardValidator">validator for <see cref="CreateCardModel"/></param>
        /// <param name="filterValidator">validator for <see cref="CardSearchFilterModel"/></param>
        /// <param name="updateCardValidator">validator for <see cref="UpdateCardModel"/></param>
        /// <exception cref="ArgumentNullException">Any of the dependencies are <see langword="null"/></exception>
        public CardService(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            IAuthenticatedUserService authUser,
            IDateTimeProvider dateTime,
            AbstractValidator<CreateCardModel> createCardValidator,
            AbstractValidator<CardSearchFilterModel> filterValidator,
            AbstractValidator<UpdateCardModel> updateCardValidator)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _authUser = authUser ?? throw new ArgumentNullException(nameof(authUser));
            _dateTime = dateTime ?? throw new ArgumentNullException(nameof(dateTime));
            _createCardValidator = createCardValidator ?? throw new ArgumentNullException(nameof(dateTime));
            _filterValidator = filterValidator ?? throw new ArgumentNullException(nameof(filterValidator));
            _updateCardValidator = updateCardValidator ?? throw new ArgumentNullException(nameof(updateCardValidator));

            _cardRepository = _unitOfWork.CardRepository;
            _keywordRepository = _unitOfWork.KeywordRepository;
        }

        /// <inheritdoc/>
        public Task<IEnumerable<CardModel>> GetAllAsync(bool includeUnverified = false, int limit = 10, int offset = 0)
        {
            if (limit <= 0)
            {
                throw new ArgumentException("limit can't be negative or zero", nameof(limit));
            }

            if (offset < 0)
            {
                throw new ArgumentException("offset can't be negative", nameof(offset));
            }

            return GetAllInternalAsync(includeUnverified, limit, offset);

            async Task<IEnumerable<CardModel>> GetAllInternalAsync(bool includeUnverified, int limit, int offset)
            {
                var cards = await _cardRepository.GetAllAsync(includeUnverified, limit, offset);

                return _mapper.Map<IEnumerable<CardModel>>(cards);
            }
        }

        /// <inheritdoc/>
        public Task<IEnumerable<CardModel>> GetByFilterAsync(CardSearchFilterModel filter, int limit = 10, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(filter);
            _filterValidator.ValidateAndThrow(filter);

            if (limit <= 0)
            {
                throw new ArgumentException("limit can't be negative or zero", nameof(limit));
            }

            if (offset < 0)
            {
                throw new ArgumentException("offset can't be negative", nameof(offset));
            }

            return GetByFilterInternalAsync(filter, limit, offset);

            async Task<IEnumerable<CardModel>> GetByFilterInternalAsync(CardSearchFilterModel filter, int limit, int offset)
            {
                var cards = await _cardRepository.GetByFilterAsync(_mapper.Map<CardSearchFilter>(filter), limit, offset);

                return _mapper.Map<IEnumerable<CardModel>>(cards);
            }
        }

        /// <inheritdoc/>
        public async Task<CardModel> GetByIdAsync(int id)
        {
            var card = await _cardRepository.GetByIdAsync(id);

            return _mapper.Map<CardModel>(card);
        }

        /// <inheritdoc/>
        public async Task<string> GetContentByIdAsync(int id)
        {
            var card = await _cardRepository.GetByIdWithContentAsync(id);

            if (card is null)
                return null;

            return card.Content.Content;
        }

        /// <inheritdoc/>
        public async Task<CardModel> AddAsync(CreateCardModel card)
        {
            ArgumentNullException.ThrowIfNull(card);
            _createCardValidator.ValidateAndThrow(card);

            var keywordEntities = await GetKeywordsFromNames(card.Keywords);
            ThrowIfKeywordDoesntExist(card.Keywords, keywordEntities, nameof(card.Keywords));

            var cardEntity = new Card
            {
                Title = card.Title,
                AuthorId = _authUser.UserId,
                CreatedAt = _dateTime.UtcNow,
                Verified = false,
                Content = new CardContent
                {
                    Content = card.Content,
                },
                Keywords = keywordEntities.ToList(),
            };

            await _cardRepository.AddAsync(cardEntity);
            await _unitOfWork.SaveAsync();

            // Get entity from repo to include author entity
            cardEntity = await _cardRepository.GetByIdAsync(cardEntity.Id);

            return _mapper.Map<CardModel>(cardEntity);
        }

        /// <inheritdoc/>
        public async Task UpdateAsync(UpdateCardModel card)
        {
            ArgumentNullException.ThrowIfNull(card);
            _updateCardValidator.ValidateAndThrow(card);

            var keywordEntities = await GetKeywordsFromNames(card.Keywords);
            ThrowIfKeywordDoesntExist(card.Keywords, keywordEntities, nameof(card.Keywords));

            var entity = await _cardRepository.GetByIdAsync(card.Id);

            if (entity is null)
                throw new EntityNotFoundException();

            entity.Title = card.Title;
            entity.Keywords = keywordEntities.ToList();

            _cardRepository.Update(entity);
            await _unitOfWork.SaveAsync();
        }

        /// <inheritdoc/>
        public async Task MarkVerified(int id)
        {
            var card = await _cardRepository.GetByIdAsync(id);

            if (card is null)
                throw new EntityNotFoundException();

            card.Verified = true;

            _cardRepository.Update(card);
            await _unitOfWork.SaveAsync();
        }

        /// <inheritdoc/>
        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _cardRepository.GetByIdAsync(id);

            if (entity is null)
                throw new EntityNotFoundException();

            _cardRepository.Delete(entity);
            await _unitOfWork.SaveAsync();
        }

        /// <inheritdoc/>
        public Task<int> Count(bool countUnverified = false)
        {
            return _cardRepository.Count(countUnverified);
        }

        /// <inheritdoc/>
        public Task<int> CountFiltered(CardSearchFilterModel filter)
        {
            ArgumentNullException.ThrowIfNull(filter);
            _filterValidator.ValidateAndThrow(filter);

            return _cardRepository.CountFiltered(_mapper.Map<CardSearchFilter>(filter));
        }

        private async Task<IEnumerable<Keyword>> GetKeywordsFromNames(IEnumerable<string> keywordNames)
        {
            if (keywordNames is null)
            {
                return Enumerable.Empty<Keyword>();
            }

            return await _keywordRepository.GetByNamesAsync(keywordNames);
        }

        /// <summary>
        /// Throws <see cref="ValidationException"/> if any of the keyword names in <paramref name="keywords"/> doesn't have
        /// <see cref="Keyword"/> entity with matching name in <paramref name="keywordEntities"/>.
        /// </summary>
        /// <param name="keywords">keyword names</param>
        /// <param name="keywordEntities">keyword entities</param>
        /// <param name="propertyName">name of the property that contained keyword names</param>
        /// <exception cref="ValidationException">At least one keyword name doesn't have a matching entity.</exception>
        private void ThrowIfKeywordDoesntExist(IEnumerable<string> keywords, IEnumerable<Keyword> keywordEntities, string propertyName)
        {
            var missingKeywords = keywords.Where(name => !keywordEntities.Any(k => k.Name == name));

            if (!missingKeywords.Any())
            {
                return;
            }

            var errors = keywords
                .Select((keyword, index) => (keyword, index))
                .Where(item => missingKeywords.Contains(item.keyword))
                .Select(item => new ValidationFailure($"{propertyName}[{item.index}]", $"Keyword '{item.keyword}' doesn't exist.", item.keyword))
                .ToList();

            throw new ValidationException(errors);
        }
    }
}
