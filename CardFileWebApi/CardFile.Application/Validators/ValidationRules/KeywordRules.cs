﻿using CardFile.Application.Models;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Validators.ValidationRules
{
    /// <summary>
    /// Contains extension methods that define validation rules for keyword properties.
    /// </summary>
    public static class KeywordRules
    {
        /// <summary>
        /// Defines validation rules for keyword name.
        /// </summary>
        /// <typeparam name="T">type of object being validated</typeparam>
        /// <param name="ruleBuilder">the rule builder on which the validator should be defined</param>
        /// <returns>A reference to this instance after the operation has completed.</returns>
        public static IRuleBuilderOptions<T, string> ValidKeywordName<T>(this IRuleBuilder<T, string> ruleBuilder)
        {
            return ruleBuilder
                .NotEmpty()
                .MaximumLength(20)
                .Matches("^[a-zA-Z0-9_]+$");
        }
    }
}
