﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CardFile.Persistence.Migrations
{
    public partial class AddAdminOnMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "DisplayName", "FirsName", "LastName" },
                values: new object[] { "6f193093-c9e1-42f4-8b93-1bd154ca9c0b", "Admin", null, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: "6f193093-c9e1-42f4-8b93-1bd154ca9c0b");
        }
    }
}
