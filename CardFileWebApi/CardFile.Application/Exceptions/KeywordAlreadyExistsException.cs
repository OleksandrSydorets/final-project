﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Exceptions
{
    /// <summary>
    /// The exception that is thrown when trying to add keyword with name that already exists.
    /// </summary>
    [Serializable]
    public class KeywordAlreadyExistsException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KeyNotFoundException"/> class.
        /// </summary>
        public KeywordAlreadyExistsException() : base("Keyword already exists.")
        {
        }

        /// <inheritdoc/>
        protected KeywordAlreadyExistsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
