using CardFile.Application;
using CardFile.Application.RequiredInterfaces;
using CardFile.Identity;
using CardFile.Identity.Persistence;
using CardFile.Persistence;
using CardFile.WebApi.Filters;
using CardFile.WebApi.Mapping;
using CardFile.WebApi.Models.Requests;
using CardFile.WebApi.Services;
using CardFile.WebApi.Validation;
using FluentValidation;
using Microsoft.OpenApi.Models;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddCardFileApplication();
builder.Services.AddCardFilePersistence(builder.Configuration);
builder.Services.AddCardFileIdentity(builder.Configuration);

builder.Services.AddHttpContextAccessor();
builder.Services.AddTransient<IAuthenticatedUserService, AuthenticatedUserService>();

builder.Services.AddTransient<AbstractValidator<CardSearchQueryParameters>, CardSearchQueryParametersValidator>();
builder.Services.AddTransient<AbstractValidator<UserSearchQueryParameters>, UserSearchQueryParametersValidator>();
builder.Services.AddAutoMapper(configuration => configuration.AddProfile<AutomapperProfile>());

builder.Services.AddControllers(options =>
{
    options.Filters.Add<ValidationExceptionFilter>();
});

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(policy =>
    {
        policy.WithOrigins(builder.Configuration["JwtOptions:ValidAudience"])
            .AllowAnyHeader()
            .AllowAnyMethod();
    });
});

builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "CardFile Api"
    });

    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));

    options.AddSecurityDefinition(name: "Bearer", securityScheme: new OpenApiSecurityScheme
    {
        Name = "Authorization",
        Description = "Enter the Bearer Authorization string as following: `Bearer Generated-JWT-Token`",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer"
    });

    options.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Name = "Bearer",
                In = ParameterLocation.Header,
                Reference = new OpenApiReference
                {
                    Id = "Bearer",
                    Type = ReferenceType.SecurityScheme
                }
            },
            new List<string>()
        }
    });
});

var app = builder.Build();

// Configure th HTTP request pipeline.
app.UseCors();

app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
