export interface PaginatedResponse {
    limit: number;
    offset: number;
    totalCount: number;
}