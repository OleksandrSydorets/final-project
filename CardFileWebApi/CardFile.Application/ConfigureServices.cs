﻿using CardFile.Application.Interfaces;
using CardFile.Application.Mapping;
using CardFile.Application.Models;
using CardFile.Application.RequiredInterfaces;
using CardFile.Application.Services;
using CardFile.Application.Validators;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application
{
    /// <summary>
    /// Contains extension method for adding <see cref="Application"/> services.
    /// </summary>
    public static class ConfigureServices
    {
        /// <summary>
        /// Adds <see cref="Application"/> services.
        /// </summary>
        /// <param name="services">the <see cref="IServiceCollection"/> to add services to</param>
        /// <returns>A reference to this instance after the operation has completed.</returns>
        public static IServiceCollection AddCardFileApplication(this IServiceCollection services)
        {
            services.AddScoped<ICardService, CardService>();
            services.AddScoped<IKeywordService, KeywordService>();
            services.AddScoped<IAccountService, AccountService>();

            services.AddTransient<IDateTimeProvider, DateTimeProvider>();

            services.AddTransient<AbstractValidator<KeywordModel>, KeywordValidator>();
            services.AddTransient<AbstractValidator<CreateKeywordModel>, CreateKeywordValidator>();
            services.AddTransient<AbstractValidator<CreateCardModel>, CreateCardValidator>();
            services.AddTransient<AbstractValidator<UpdateCardModel>, UpdateCardValidator>();
            services.AddTransient<AbstractValidator<CardSearchFilterModel>, CardSearchFilterValidator>();
            services.AddTransient<AbstractValidator<UserSearchFilterModel>, UserSearchFilterValidator>();

            services.AddAutoMapper(configuration => configuration.AddProfile<AutomapperProfile>());

            ValidatorOptions.Global.LanguageManager.Enabled = false;

            return services;
        }
    }
}
