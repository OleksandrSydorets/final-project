﻿using AutoMapper;
using CardFile.Application.Models;
using CardFile.Identity.Entities;
using System.Linq;

namespace CardFile.Identity.Mapping
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<CardFileUser, UserModel>()
                .ForMember(d => d.Role, s => s.MapFrom(s => s.Roles.Single().Name));
        }
    }
}
