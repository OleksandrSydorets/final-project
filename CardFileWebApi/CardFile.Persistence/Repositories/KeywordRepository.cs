﻿using CardFile.Domain.Entities;
using CardFile.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Persistence.Repositories
{
    /// <summary>
    /// An <see cref="IKeywordRepository"/> implementation.
    /// </summary>
    public class KeywordRepository : IKeywordRepository
    {
        private readonly DbSet<Keyword> _keywords;

        /// <summary>
        ///  Initializes a new instance of the <see cref="KeywordRepository"/> class.
        /// </summary>
        /// <param name="dbContext">database context</param>
        /// <exception cref="ArgumentException">
        /// <paramref name="dbContext"/> is <see langword="null"/>
        /// </exception>
        public KeywordRepository(CardFileDbContext dbContext)
        {
            ArgumentNullException.ThrowIfNull(dbContext);

            _keywords = dbContext.Keywords;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<Keyword>> GetAllAsync()
        {
            return await _keywords
                .OrderBy(k => k.Name)
                .ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<Keyword>> GetByNamesAsync(IEnumerable<string> names)
        {
            return await _keywords
                .OrderBy(k => k.Name)
                .Where(k => names.Contains(k.Name))
                .ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<Keyword> GetByIdAsync(int id)
        {
            return await _keywords.FindAsync(id); 
        }

        /// <inheritdoc/>
        public async Task<Keyword> GetByNameAsync(string name)
        {
            return await _keywords.FirstOrDefaultAsync(k => k.Name == name);
        }

        /// <inheritdoc/>
        public async Task AddAsync(Keyword keyword)
        {
            await _keywords.AddAsync(keyword);
        }

        /// <inheritdoc/>
        public void Update(Keyword keyword)
        {
            _keywords.Update(keyword);
        }

        /// <inheritdoc/>
        public void Delete(Keyword keyword)
        {
            _keywords.Remove(keyword);
        }

        /// <inheritdoc/>
        public async Task DelelteByIdAsync(int id)
        {
            var keyword = await _keywords.FindAsync(id);

            if (keyword is not null)
                _keywords.Remove(keyword);
        }
    }
}
