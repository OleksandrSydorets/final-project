﻿using CardFile.Application.Validators.ValidationRules;
using CardFile.WebApi.Models.Requests;
using FluentValidation;

namespace CardFile.WebApi.Validation
{
    /// <summary>
    /// <see cref="FluentValidation"/> validator for <see cref="CardSearchQueryParameters"/>.
    /// </summary>
    public class CardSearchQueryParametersValidator : AbstractValidator<CardSearchQueryParameters>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CardSearchQueryParametersValidator"/> class.
        /// </summary>
        public CardSearchQueryParametersValidator()
        {
            RuleFor(m => m.Keywords)
                .ForEach(k => k.ValidKeywordName());

            RuleFor(m => m.Author)
                .Must(BeNullOrEmptyIfAuthorIsNotNullOrEmpty)
                .WithMessage($"Properties '{nameof(CardSearchQueryParameters.Author)}' and '{nameof(CardSearchQueryParameters.AuthorId)}' cannot be both specified.");

            RuleFor(m => m.Limit)
                .GreaterThan(0);

            RuleFor(m => m.Offset)
                .GreaterThanOrEqualTo(0);
        }

        private bool BeNullOrEmptyIfAuthorIsNotNullOrEmpty(CardSearchQueryParameters parameters, string author)
        {
            return string.IsNullOrEmpty(parameters.AuthorId) || string.IsNullOrEmpty(author);
        }
    }
}
