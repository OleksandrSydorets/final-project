﻿using CardFile.Application.Models;
using CardFile.Identity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Identity.Helpers
{
    /// <summary>
    /// Contains extension method for work with <see cref="UserSearchFilterModel"/>.
    /// </summary>
    public static class FilterExtensions
    {
        /// <summary>
        /// Applies <see cref="UserSearchFilterModel"/> to an <see cref="IQueryable{T}"/> of <see cref="CardFileUser"/>.
        /// </summary>
        /// <param name="query">The users query</param>
        /// <param name="filter">The filter</param>
        /// <returns>Queryable with filter applied.</returns>
        public static IQueryable<CardFileUser> ApplyFilter(this IQueryable<CardFileUser> query, UserSearchFilterModel filter)
        {
            if (filter == null)
            {
                return query;
            }

            if (!string.IsNullOrWhiteSpace(filter.UserName))
            {
                query = query.Where(x => x.UserName.Contains(filter.UserName));
            }

            if (!string.IsNullOrWhiteSpace(filter.Role))
            {
                query = query.Where(x => x.Roles.Any(role => role.Name == filter.Role));
            }

            return query;
        }
    }
}
