﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CardFile.Identity.Migrations
{
    public partial class AddModeratorRole : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "a7d31612-67ec-4f56-ba97-c39e2ef77ea5", "27d9f92f-ce97-4bf2-9ab6-1ce174b50489", "Moderator", "MODERATOR" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "a7d31612-67ec-4f56-ba97-c39e2ef77ea5");
        }
    }
}
