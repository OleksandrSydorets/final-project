﻿using AutoMapper;
using CardFile.Application.Models;
using CardFile.WebApi.Models.Requests;

namespace CardFile.WebApi.Mapping
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<CardSearchQueryParameters, CardSearchFilterModel>();

            CreateMap<UserSearchQueryParameters, UserSearchFilterModel>();
        }
    }
}
