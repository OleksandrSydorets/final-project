﻿using CardFile.Domain.Entities;
using CardFile.Domain.Repositories;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Persistence.Repositories
{
    /// <summary>
    /// An <see cref="IUserRepository"/> implementation.
    /// </summary>
    public class UserRepository : IUserRepository
    {
        private readonly DbSet<User> _users;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="dbContext">database context</param>
        /// <exception cref="ArgumentException">
        /// <paramref name="dbContext"/> is <see langword="null"/>
        /// </exception>
        public UserRepository(CardFileDbContext dbContext)
        {
            ArgumentNullException.ThrowIfNull(dbContext);

            _users = dbContext.Users;
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<User>> GetAllAsync()
        {
            return await _users.ToListAsync();
        }

        /// <inheritdoc/>
        public async Task<User> GetByIdAsync(int id)
        {
            return await _users.FindAsync(id);
        }

        /// <inheritdoc/>
        public async Task AddAsync(User user)
        {
            await _users.AddAsync(user);
        }

        /// <inheritdoc/>
        public void Update(User user)
        {
            _users.Update(user);
        }

        /// <inheritdoc/>
        public void Delete(User user)
        {
            _users.Remove(user);
        }

        /// <inheritdoc/>
        public async Task DelelteByIdAsync(int id)
        {
            var user = await _users.FindAsync(id);

            if (user is not null)
                _users.Remove(user);
        }
    }
}
