﻿using CardFile.Application.Models;
using CardFile.Application.Validators.ValidationRules;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Validators
{
    /// <summary>
    /// <see cref="FluentValidation"/> validator for <see cref="CreateCardModel"/>.
    /// </summary>
    public class CreateCardValidator : AbstractValidator<CreateCardModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateCardValidator"/> class.
        /// </summary>
        public CreateCardValidator()
        {
            RuleFor(m => m.Title).ValidCardTitle();

            RuleFor(m => m.Content).ValidCardContent();

            RuleFor(m => m.Keywords).ValidCardKeywords();
        }
    }
}
