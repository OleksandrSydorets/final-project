﻿using AutoMapper;
using CardFile.Application.Exceptions;
using CardFile.Application.Models;
using CardFile.Application.RequiredInterfaces;
using CardFile.Identity.Entities;
using CardFile.Identity.Helpers;
using CardFile.Identity.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Identity.Services
{
    /// <summary>
    /// An <see cref="IIdentityService"/> implementation.
    /// </summary>
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<CardFileUser> _userManager;
        private readonly IMapper _mapper;
        private readonly JwtOptions _jwtOptions;

        /// <summary>
        /// Initializes a new instance of the <see cref="IdentityService"/> class.
        /// </summary>
        /// <param name="userManager">Identity user manager</param>
        /// <param name="jwtOptions">JWT options</param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="userManager"/> or <paramref name="jwtOptions"/> is null.
        /// </exception>
        public IdentityService(UserManager<CardFileUser> userManager, IOptions<JwtOptions> jwtOptions, IMapper mapper)
        {
            _userManager = userManager ?? throw new ArgumentNullException(nameof(userManager));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _jwtOptions = jwtOptions?.Value ?? throw new ArgumentNullException(nameof(jwtOptions));
        }

        /// <inheritdoc/>
        public async Task ChangePasswordAsync(string userName, string currentPassword, string newPassword)
        {
            ArgumentNullException.ThrowIfNull(userName);
            ArgumentNullException.ThrowIfNull(currentPassword);
            ArgumentNullException.ThrowIfNull(newPassword);

            var user = await _userManager.FindByNameAsync(userName);
            var changePasswordResult = await _userManager.ChangePasswordAsync(user, currentPassword, newPassword);
            ThrowAuthenticationExceptionIfResultFailed(changePasswordResult);
        }

        /// <inheritdoc/>
        public async Task<string> LoginAsync(string userName, string password)
        {
            ArgumentNullException.ThrowIfNull(userName);
            ArgumentNullException.ThrowIfNull(password);

            var user = await _userManager.FindByNameAsync(userName);
            bool correctPassword = await _userManager.CheckPasswordAsync(user, password);

            if (!correctPassword)
            {
                throw new AuthenticationException(new string[] { "UserName or password incorrect." });
            }

            return await GenerateTokenAsync(user);
        }

        /// <inheritdoc/>
        public async Task<(string token, string userId)> RegisterAsync(string userName, string password)
        {
            ArgumentNullException.ThrowIfNull(userName);
            ArgumentNullException.ThrowIfNull(password);

            var user = new CardFileUser(userName);
            var createResult = await _userManager.CreateAsync(user, password);
            ThrowAuthenticationExceptionIfResultFailed(createResult);

            var addToRoleResult = await _userManager.AddToRoleAsync(user, UserRoles.User);
            ThrowAuthenticationExceptionIfResultFailed(addToRoleResult);

            string token = await GenerateTokenAsync(user);
            return (token, user.Id);
        }

        /// <inheritdoc/>
        public async Task ChangeRoleAsync(string userId, string role)
        {
            ArgumentNullException.ThrowIfNull(userId);
            ArgumentNullException.ThrowIfNull(role);

            if (!UserRoles.Roles.Contains(role))
            {
                throw new AuthenticationException(new string[] { $"Role '{role}' doesn't exist." });
            }

            var user = await _userManager.FindByIdAsync(userId);

            if (user is null)
            {
                throw new AuthenticationException(new string[] { $"User with id '{userId}' doesn't exist." });
            }

            var currentRoles = await _userManager.GetRolesAsync(user);
            var removeRolesResult = await _userManager.RemoveFromRolesAsync(user, currentRoles);
            ThrowAuthenticationExceptionIfResultFailed(removeRolesResult);

            var addToRoleResult = await _userManager.AddToRoleAsync(user, role);
            ThrowAuthenticationExceptionIfResultFailed(addToRoleResult);
        }

        /// <inheritdoc/>
        public async Task<IEnumerable<UserModel>> GetUsersAsync(int limit = 10, int offset = 0, UserSearchFilterModel filter = null)
        {
            var users = await _userManager.Users
                .Include(user => user.Roles)
                .ApplyFilter(filter)
                .OrderBy(user => user.UserName)
                .Skip(offset)
                .Take(limit)
                .ToListAsync();

            return _mapper.Map<IEnumerable<UserModel>>(users);
        }

        /// <inheritdoc/>
        public async Task<int> GetUserCountAsync(UserSearchFilterModel filter = null)
        {
            return await _userManager.Users
                .ApplyFilter(filter)
                .CountAsync();
        }

        /// <inheritdoc/>
        public async Task<UserModel> GetUserByIdAsync(string id)
        {
            ArgumentNullException.ThrowIfNull(id);

            var user = await _userManager.Users
                .Include(user => user.Roles)
                .FirstOrDefaultAsync(user => user.Id == id);

            return _mapper.Map<UserModel>(user);
        }

        private async Task<string> GenerateTokenAsync(CardFileUser user)
        {
            var claims = new List<Claim>
            {
                new Claim("id", user.Id),
                new Claim(ClaimTypes.Name, user.UserName),
            };

            var roles = await _userManager.GetRolesAsync(user);
            claims.AddRange(roles.Select(role => new Claim(ClaimTypes.Role, role)));

            var key = _jwtOptions.GetSymmetricSecurityKey();
            var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            DateTime now = DateTime.UtcNow;
            DateTime expires = now.AddMinutes(_jwtOptions.ValidDurationInMinutes);

            var tokenDescriptor = new SecurityTokenDescriptor()
            {
                Issuer = _jwtOptions.ValidIssuer,
                Audience = _jwtOptions.ValidAudience,
                Subject = new ClaimsIdentity(claims),
                IssuedAt = now,
                NotBefore = now,
                Expires = expires,
                SigningCredentials = signingCredentials,
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            
            return tokenHandler.WriteToken(token);
        }

        private static void ThrowAuthenticationExceptionIfResultFailed(IdentityResult result)
        {
            if (!result.Succeeded)
            {
                throw new AuthenticationException(result.Errors.Select(e => e.Description));
            }
        }
    }
}
