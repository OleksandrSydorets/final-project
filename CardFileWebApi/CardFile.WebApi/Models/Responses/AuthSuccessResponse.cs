﻿namespace CardFile.WebApi.Models.Responses
{
    public class AuthSuccessResponse
    {
        public AuthSuccessResponse(string token)
        {
            Token = token;
        }

        public string Token { get; }
    }
}
