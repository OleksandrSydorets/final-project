﻿using AutoMapper;
using CardFile.Application.Exceptions;
using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using CardFile.WebApi.Models.Requests;
using CardFile.WebApi.Models.Responses;
using CardFile.WebApi.Validation;
using FluentValidation;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;
using System.Net.Mime;
using System.Text.Json;

namespace CardFile.WebApi.Controllers
{
    /// <summary>
    /// Provides endpoints for adding, updating, deleting and retrieving index cards.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CardsController : ControllerBase
    {
        private readonly ICardService _cardService;
        private readonly AbstractValidator<CardSearchQueryParameters> _searchQueryValidator;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CardsController"/> class.
        /// </summary>
        /// <param name="cardService">The card service</param>
        /// <param name="searchQueryValidator">Search query parameters validator</param>
        /// <param name="mapper">Mapper</param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="cardService"/>, <paramref name="searchQueryValidator"/> or <paramref name="mapper"/>
        /// is <see langword="null"/>.
        /// </exception>
        public CardsController(ICardService cardService, AbstractValidator<CardSearchQueryParameters> searchQueryValidator, IMapper mapper)
        {
            _cardService = cardService ?? throw new ArgumentNullException(nameof(cardService));
            _searchQueryValidator = searchQueryValidator ?? throw new ArgumentNullException(nameof(searchQueryValidator));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Retrieves index cards.
        /// </summary>
        /// <param name="parameters">The filter</param>
        /// <returns>
        /// Response with cards or validation errors.
        /// </returns>
        /// <exception cref="ValidationException"><paramref name="parameters"/> are invalid.</exception>
        /// <response code="200">Response containing index cards</response>
        /// <response code="400">Response containing validation errors</response>
        [SwaggerResponse((int)HttpStatusCode.OK, type: typeof(CardSearchResponse), contentTypes: new[] { MediaTypeNames.Application.Json })]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, type: typeof(ValidationErrorResponse), contentTypes: new[] { MediaTypeNames.Application.Json })]
        [HttpGet]
        public async Task<ActionResult<CardSearchResponse>> Get([FromQuery] CardSearchQueryParameters parameters)
        {
            _searchQueryValidator.ValidateAndThrow(parameters);

            IEnumerable<CardModel> cards;
            int totalCount;

            if (parameters.IsEmpty())
            {
                totalCount = await _cardService.Count();
                cards = await _cardService.GetAllAsync(limit: parameters.Limit, offset: parameters.Offset);
            }
            else
            {
                var filter = _mapper.Map<CardSearchFilterModel>(parameters);
                totalCount = await _cardService.CountFiltered(filter);
                cards = await _cardService.GetByFilterAsync(filter, parameters.Limit, parameters.Offset);
            }

            var response = new CardSearchResponse
            {
                Cards = cards,
                Limit = parameters.Limit,
                Offset = parameters.Offset,
                TotalCount = totalCount
            };

            return Ok(response);
        }

        /// <summary>
        /// Retrieves an index card by id.
        /// </summary>
        /// <param name="id">The card id</param>
        /// <returns>
        /// Response with the index card or NotFound.
        /// </returns>
        /// <response code="200">Response containing the index card</response>
        /// <response code="404">Card not found</response>
        [HttpGet("{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, type: typeof(CardModel), contentTypes: new[] { MediaTypeNames.Application.Json })]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<CardModel>> Get(int id)
        {
            var card = await _cardService.GetByIdAsync(id);

            if (card is null)
                return NotFound();

            return Ok(card);
        }

        /// <summary>
        /// Retrieves and index card's content by index card's id.
        /// </summary>
        /// <param name="cardId">The card id</param>
        /// <returns>
        /// Content or NotFound.
        /// </returns>
        /// <response code="200">Card's content</response>
        /// <response code="404">Card not found</response>
        [HttpGet("{cardId}/content")]
        [SwaggerResponse((int)HttpStatusCode.OK, type: typeof(string), contentTypes: new[] { MediaTypeNames.Text.Plain })]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<string>> GetContent(int cardId)
        {
            string content = await _cardService.GetContentByIdAsync(cardId);

            if (content is null)
                return NotFound();

            return Ok(content);
        }

        /// <summary>
        /// Creates a new index card.
        /// </summary>
        /// <param name="model">The card</param>
        /// <returns>
        /// Response with the newly created index card or validation error response.
        /// </returns>
        /// <response code="201">Response containing the newly created index card</response>
        /// <response code="400">Response containing validation errors</response>
        [HttpPost]
        [Authorize]
        [SwaggerResponse((int)HttpStatusCode.Created, type: typeof(CardModel), contentTypes: new[] { MediaTypeNames.Application.Json })]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, type: typeof(ValidationErrorResponse), contentTypes: new[] { MediaTypeNames.Application.Json })]
        public async Task<ActionResult<CardModel>> Post(CreateCardModel model)
        {
            var card = await _cardService.AddAsync(model);

            return CreatedAtAction("Get", new { id = card.Id }, card);
        }

        /// <summary>
        /// Updates an existing index card.
        /// </summary>
        /// <param name="id">The card id</param>
        /// <param name="request">The card</param>
        /// <returns>
        /// Response with the updated index card, NotFound or validation error response.
        /// </returns>
        /// <response code="200">Response containing an updated index card</response>
        /// <response code="400">Response containing validation errors</response>
        /// <response code="404">Card not found</response>
        [HttpPut("{id}")]
        [Authorize(Roles = UserRoles.AdminOrModerator)]
        [SwaggerResponse((int)HttpStatusCode.OK, type: typeof(CardModel), contentTypes: new[] { MediaTypeNames.Application.Json })]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, type: typeof(ValidationErrorResponse), contentTypes: new[] { MediaTypeNames.Application.Json })]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<CardModel>> Put(int id, [FromBody] UpdateCardRequest request)
        {
            try
            {
                var model = new UpdateCardModel
                {
                    Id = id,
                    Title = request.Title,
                    Keywords = request.Keywords
                };

                await _cardService.UpdateAsync(model);

                var card = await _cardService.GetByIdAsync(id);

                return Ok(card);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Marks an index card as verified.
        /// </summary>
        /// <param name="id">The card id</param>
        /// <returns>
        /// Response with updated index card or NotFound.
        /// </returns>
        /// <response code="200">Response containing an updated index card</response>
        /// <response code="404">Card not found</response>
        [HttpPost("{id}/mark-verified")]
        [Authorize(Roles = UserRoles.AdminOrModerator)]
        [SwaggerResponse((int)HttpStatusCode.OK, type: typeof(CardModel), contentTypes: new[] { MediaTypeNames.Application.Json })]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<CardModel>> MarkVerified(int id)
        {
            try
            {
                await _cardService.MarkVerified(id);

                var card = await _cardService.GetByIdAsync(id);

                return Ok(card);
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Deletes an index card.
        /// </summary>
        /// <param name="id">The card id</param>
        /// <returns>NoContent or NotFound</returns>
        /// <response code="204">Card successfully deleted</response>
        /// <response code="404">Card not found</response>
        [HttpDelete("{id}")]
        [Authorize(Roles = UserRoles.AdminOrModerator)]
        [SwaggerResponse((int)HttpStatusCode.NoContent)]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                await _cardService.DeleteByIdAsync(id);

                return NoContent();
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
        }
    }
}
