﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Models
{
    /// <summary>
    /// Data transfer object that contains properties need to create a keyword.
    /// </summary>
    public class CreateKeywordModel
    {
        public string Name { get; set; }
    }
}
