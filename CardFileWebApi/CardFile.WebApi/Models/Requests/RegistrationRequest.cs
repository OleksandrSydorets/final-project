﻿using System.ComponentModel.DataAnnotations;

namespace CardFile.WebApi.Models.Requests
{
    public class RegistrationRequest
    {

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
