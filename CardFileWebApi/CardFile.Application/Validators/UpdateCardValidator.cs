﻿using CardFile.Application.Models;
using CardFile.Application.Validators.ValidationRules;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Validators
{
    /// <summary>
    /// <see cref="FluentValidation"/> valiadtor for <see cref="UpdateCardModel"/>.
    /// </summary>
    public class UpdateCardValidator : AbstractValidator<UpdateCardModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UpdateCardValidator"/> class.
        /// </summary>
        public UpdateCardValidator()
        {
            RuleFor(m => m.Title).ValidCardTitle();

            RuleFor(m => m.Keywords).ValidCardKeywords();
        }
    }
}
