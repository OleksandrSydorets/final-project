import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { PaginatedResponse } from 'src/app/api-contracts/results/paginated-response';
import { Page, PaginationItem, PrevNextPage, PrevNextPageEnabled, PrevNextPageDisabled, SkipedPageRange } from 'src/app/models/pagination';

@Component({
  selector: 'app-search-result-navigation',
  templateUrl: './search-result-navigation.component.html',
  styleUrls: ['./search-result-navigation.component.css']
})
export class SearchResultNavigationComponent implements OnInit {
  @Input() searchResult: PaginatedResponse | null = null;
  @Input() pluralItemName: string = 'items';
  @Output() changePageClick = new EventEmitter<number>();

  pageItems!: PaginationItem[];
  prevPage!: PrevNextPage;
  nextPage!: PrevNextPage;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.createPages();
  }

  createPages(): void {
    const pageCount = Math.ceil(this.searchResult!.totalCount / this.searchResult!.limit);
    const currentPage = this.currentPage!;
    const currMinus2 = currentPage - 2;
    const currPlus2 = currentPage + 2;
    const url = this.getUrl();

    this.pageItems =  [...Array(pageCount).keys()]
      .map(i => i + 1)
      .reduce((pages, page) => {
        
        if (page === 1 || page === pageCount || (page >= currMinus2 && page <= currPlus2)) {
          const active = page === currentPage;
          const queryParams = this.getParams(page);
          pages.push(new Page(page, active, url, queryParams));
        } 
        else if (pages[pages.length-1] instanceof Page) {
          pages.push(new SkipedPageRange());
        }

        return pages;
      }, <PaginationItem[]>[]);
    
    if (currentPage > 1) {
      const queryParams = this.getParams(currentPage - 1);
      this.prevPage = new PrevNextPageEnabled(url, queryParams, () => this.onChangePageClick(currentPage - 1))
    } else {
      this.prevPage = new PrevNextPageDisabled();
    }
    
    if (currentPage < pageCount) {
      const queryParams = this.getParams(currentPage + 1);
      this.nextPage = new PrevNextPageEnabled(url, queryParams, () => this.onChangePageClick(currentPage + 1))
    } else {
      this.nextPage = new PrevNextPageDisabled();
    }
  }

  get currentPage(): number | null {
    if (!this.searchResult) {
      return null;
    }

    return this.searchResult.offset / this.searchResult.limit + 1;
  }

  get from() {
    if (!this.searchResult) {
      return null;
    }
    return this.searchResult.offset + 1;
  }

  get to() {
    if (!this.searchResult) {
      return null;
    }

    const offsetPlusLimit = this.searchResult.offset + this.searchResult.limit;
    return Math.min(this.searchResult.totalCount, offsetPlusLimit);
  }

  get total() {
    if (!this.searchResult) {
      return null;
    }
    return this.searchResult.totalCount;
  }

  get showNavigation(): boolean {
    return this.searchResult ? this.searchResult.totalCount > 0 : false;
  }

  get showPages(): boolean {
    return this.searchResult ? this.searchResult.totalCount > this.searchResult.limit : false;
  }

  getUrl() {
    return this.route.snapshot.url;
  }

  getParams(page: number) {
    let currentParams = this.route.snapshot.queryParamMap;
    let linkParams: Params = {};

    currentParams.keys.forEach(key => {
      linkParams[key] = currentParams.getAll(key);
    })

    if (page > 1) {
      linkParams['page'] = page;
    }
    else {
      delete linkParams['page'];
    }

    return linkParams;
  }

  onChangePageClick(page: number) {
    this.changePageClick.emit(page);
  }

  pageItemCss(item: PaginationItem) {
    return { 
      active: !item.isSkipedRange && item.active, 
      disabled: item.isSkipedRange 
    };
  }

}
