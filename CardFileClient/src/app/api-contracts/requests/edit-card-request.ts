export class EditCardRequest {
    title: string;
    keywords: string[];

    constructor(title: string, keywords: string[]) {
        this.title = title;
        this.keywords = keywords;
    }
}