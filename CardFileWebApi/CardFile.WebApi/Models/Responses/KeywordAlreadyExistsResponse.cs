﻿namespace CardFile.WebApi.Models.Responses
{
    public class KeywordAlreadyExistsResponse
    {
        public string Error { get; } = "Keyword already exists.";
    }
}
