﻿using CardFile.Application.Exceptions;
using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using CardFile.WebApi.Models.Requests;
using CardFile.WebApi.Models.Responses;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Net;

namespace CardFile.WebApi.Controllers
{
    /// <summary>
    /// Provides endpoints for adding, updating, deleting and retrieving keywords.
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class KeywordsController : ControllerBase
    {
        private readonly IKeywordService _keywordService;

        /// <summary>
        /// Initializes a new instance of the <see cref="KeywordsController"/> class.
        /// </summary>
        /// <param name="keywordService">The keyword service</param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="keywordService"/> is <see langword="null"/>.
        /// </exception>
        public KeywordsController(IKeywordService keywordService)
        {
            _keywordService = keywordService ?? throw new ArgumentNullException(nameof(keywordService));
        }

        /// <summary>
        /// Retrieves keywords.
        /// </summary>
        /// <returns>
        /// Response with keywords.
        /// </returns>
        /// <response code="200">Response containing keywords</response>
        [HttpGet]
        [SwaggerResponse((int)HttpStatusCode.OK, type: typeof(IEnumerable<KeywordModel>))]
        public async Task<ActionResult<IEnumerable<KeywordModel>>> Get()
        {
            var keywords = await _keywordService.GetAllAsync();

            return Ok(keywords);
        }

        /// <summary>
        /// Retrieves a keyword by id.
        /// </summary>
        /// <param name="id">The keyword id</param>
        /// <returns>
        /// Response with the keyword or NotFound.
        /// </returns>
        /// <response code="200">Response containing the keyword</response>
        /// <response code="404">Keyword not found</response>
        [HttpGet("{id}")]
        [SwaggerResponse((int)HttpStatusCode.OK, type: typeof(KeywordModel))]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult<KeywordModel>> Get(int id)
        {
            var keyword = await _keywordService.GetByIdAsync(id);

            if (keyword is null)
                return NotFound();

            return Ok(keyword);
        }

        /// <summary>
        /// Creates a new keyword.
        /// </summary>
        /// <param name="model">The keyword</param>
        /// <returns>
        /// Response with the newly created keyword or validation error response.
        /// </returns>
        /// <response code="201">Response containing the newly created keyword</response>
        /// <response code="400">Response containing validation errors</response>
        /// <response code="409">Keyword already exists response</response>
        [HttpPost]
        [Authorize(Roles = UserRoles.AdminOrModerator)]
        [SwaggerResponse((int)HttpStatusCode.Created, type: typeof(KeywordModel))]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, type: typeof(ValidationErrorResponse))]
        [SwaggerResponse((int)HttpStatusCode.Conflict, type: typeof(KeywordAlreadyExistsResponse))]
        public async Task<ActionResult<KeywordModel>> Post([FromBody] CreateKeywordModel model)
        {
            try
            {
                var keyword = await _keywordService.AddAsync(model);

                return CreatedAtAction("Get", new { id = keyword.Id }, keyword);
            }
            catch (KeywordAlreadyExistsException)
            {
                return Conflict(new KeywordAlreadyExistsResponse());
            }
        }

        /// <summary>
        /// Updates an existing keyword.
        /// </summary>
        /// <param name="id">The keyword id</param>
        /// <param name="request">The keyword</param>
        /// <returns>
        /// NoContent, NotFound, keyword already exists response or validation error response.
        /// </returns>
        /// <response code="204">Keyword successfully updated</response>
        /// <response code="400">Response containing validation errors</response>
        /// <response code="404">Keyword not found</response>
        /// <response code="409">Keyword already exists response</response>
        [HttpPut("{id}")]
        [Authorize(Roles = UserRoles.AdminOrModerator)]
        [SwaggerResponse((int)HttpStatusCode.NoContent)]
        [SwaggerResponse((int)HttpStatusCode.BadRequest, type: typeof(ValidationErrorResponse))]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]
        [SwaggerResponse((int)HttpStatusCode.Conflict, type: typeof(KeywordAlreadyExistsResponse))]
        public async Task<ActionResult> Put(int id, [FromBody] UpdateKeywordRequest request)
        {
            try
            {
                var keyword = new KeywordModel
                {
                    Id = id,
                    Name = request.Name
                };

                await _keywordService.UpdateAsync(keyword);

                return NoContent();
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
            catch (KeywordAlreadyExistsException)
            {
                return Conflict(new KeywordAlreadyExistsResponse());
            }
        }

        /// <summary>
        /// Deletes a keyword.
        /// </summary>
        /// <param name="id">The keyword id</param>
        /// <returns>NoContent or NotFound</returns>
        /// <response code="204">Keyword successfully deleted</response>
        /// <response code="404">Keyword not found</response>
        [HttpDelete("{id}")]
        [Authorize(Roles = UserRoles.AdminOrModerator)]
        [SwaggerResponse((int)HttpStatusCode.NoContent)]
        [SwaggerResponse((int)HttpStatusCode.NotFound)]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                await _keywordService.DeleteByIdAsync(id);

                return NoContent();
            }
            catch (EntityNotFoundException)
            {
                return NotFound();
            }
        }
    }
}
