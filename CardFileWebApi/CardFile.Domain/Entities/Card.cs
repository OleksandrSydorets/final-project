﻿namespace CardFile.Domain.Entities
{
    /// <summary>
    /// Represents an index card.
    /// </summary>
    public class Card
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string AuthorId { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool Verified { get; set; }

        public CardContent Content { get; set; }
        public User Author { get; set; }
        public ICollection<Keyword> Keywords { get; set; }
    }
}
