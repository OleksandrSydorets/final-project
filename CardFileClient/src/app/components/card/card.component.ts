import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, Observable, throwError } from 'rxjs';
import { UserRole } from 'src/app/models/user-role';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Card } from '../../models/card';
import { CardService } from '../../services/card.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  cardId!: number;
  card$!: Observable<Card>;
  content$!: Observable<string>;
  deleteInProgress: boolean = false;
  error: string | null = null;
  showEditDelete: boolean = false;
  cardNotFound: boolean = false;

  @ViewChild('confirmDeleteModal') modal!: ElementRef;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private cardService: CardService,
    private authService: AuthenticationService
    ) { }

  ngOnInit(): void {
    this.cardId = this.route.snapshot.params['id'];
    this.card$ = this.cardService.getById(this.cardId)
      .pipe(
        catchError(err => {
          this.cardNotFound = true;
          return throwError(() => err);
        })
      );
    
    this.content$ = this.cardService.getContentById(this.cardId);
    this.showEditDelete = UserRole.isModeratorOrAdmin(this.authService.role);
  }

  toLocaleString(date: string) {
    return new Date(date).toLocaleString();
  }

  toComaSeparetedList(keywords: string[]) {
    return keywords.join(', ');
  }

  onDelete() {
    this.deleteInProgress = true;

    this.cardService.deleteCard(this.cardId)
      .subscribe({
        next: () => {
          this.deleteInProgress = false;
          this.router.navigate([''])
        },
        error: () => {
          this.deleteInProgress = false;
          this.error = "Failed to delete card.";
        }
      });
  }

  onMarkVerified() {
    this.cardService.markVerified(this.cardId)
      .subscribe({
        next: () => {
          this.card$ = this.cardService.getById(this.cardId);
        },
        error: () => {
          this.error = "Failed to verify card."
        }
      })
  }
}
