import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CardFilter } from '../models/card-filter';
import { ApiPaths } from '../api-paths';
import { Card } from '../models/card';
import { map, Observable } from 'rxjs';
import { CreateCardRequest } from '../api-contracts/requests/create-card-request';
import { EditCardRequest } from '../api-contracts/requests/edit-card-request';
import { CardSearchResponse } from '../api-contracts/results/card-search-response';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  constructor(private httpClient: HttpClient) { }

  getByFilter(filter: CardFilter, pageNumber: number = 1, pageSize: number = 10): Observable<CardSearchResponse> {
    const offset = (pageNumber - 1) * pageSize;

    const params = this.paramsFromFilter(filter)
      .set('limit', pageSize)
      .set('offset', offset);
    
    return this.httpClient.get<CardSearchResponse>(ApiPaths.Cards.Base, { params: params })
  }

  getById(id: number): Observable<Card> {
    return this.httpClient.get<Card>(ApiPaths.Cards.byId(id));
  }

  getContentById(id: number): Observable<string> {
    return this.httpClient.get(ApiPaths.Cards.contentById(id), { responseType: 'text' });
  }

  postCard(title: string, content: string, keywords: string[]): Observable<Card> {
    const request = new CreateCardRequest(title, content, keywords);
    return this.httpClient.post<Card>(ApiPaths.Cards.Base, request);
  }

  putCard(id: number, title: string, keywords: string[]) {
    const request = new EditCardRequest(title, keywords);
    return this.httpClient.put<Card>(ApiPaths.Cards.byId(id), request);
  }

  markVerified(id: number) {
    return this.httpClient.post<Card>(ApiPaths.Cards.verifyById(id), null);
  }

  deleteCard(id: number) {
    return this.httpClient.delete(ApiPaths.Cards.byId(id));
  }

  private paramsFromFilter(filter: CardFilter) {
    let params = new HttpParams();

    if (filter.title) {
      params = params.set('title', filter.title);
    }

    if (filter.authorId) {
      params = params.set('authorId', filter.authorId);
    }

    if (filter.author) {
      params = params.set('author', filter.author);
    }

    if (filter.includeUnverified) {
      params = params.set('includeUnverified', filter.includeUnverified);
    }

    if (filter.after) {
      params = params.set('after', filter.after.toUTCString());
    }

    if (filter.before) {
      params = params.set('before', filter.before.toUTCString());
    }

    if (filter.keywords) {
      filter.keywords.forEach(keyword => params = params.append('keywords', keyword));
    }

    return params;
  }
}
