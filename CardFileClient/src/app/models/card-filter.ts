export class CardFilter {
    title?: string;
    authorId?: string;
    author?: string;
    includeUnverified?: boolean;
    after?: Date;
    before?: Date;
    keywords?: string[];
}