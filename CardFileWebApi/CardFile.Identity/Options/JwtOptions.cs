﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace CardFile.Identity.Options
{
    /// <summary>
    /// Represents options used when creating and validation JWT tokens.
    /// </summary>
    public class JwtOptions
    {
        public const string Section = "JwtOptions";

        public string ValidIssuer { get; set; }
        public string ValidAudience { get; set; }
        public int ValidDurationInMinutes { get; set; }
        public string SecurityKey { get; set; }

        public SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(SecurityKey));
        }
    }
}
