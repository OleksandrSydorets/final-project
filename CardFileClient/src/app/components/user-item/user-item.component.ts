import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ControlValidators } from 'src/app/control-validators';
import { User } from 'src/app/models/user';
import { UserRole } from 'src/app/models/user-role';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: '[app-user-item]',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent implements OnInit {
  @Input() user!: User;

  roles = UserRole.Roles;
  selectRoleControl!: FormControl;

  constructor(
    private authService: AuthenticationService, 
    private userService: UserService, 
    private router: Router, 
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.selectRoleControl = new FormControl(this.user.role, ControlValidators.notEqualTo(this.user.role));
  }

  get selectedRole(): string {
    return this.selectRoleControl.value;
  }

  get showChangeRole(): boolean {
    return this.authService.role === UserRole.Admin;
  }

  get changeBtnDisabled(): boolean {
    return this.selectRoleControl.value === this.user.role;
  }

  onChangeRoleClick() {
    this.userService.changeRole(this.user.id, this.selectRoleControl.value)
      .subscribe({
        next: () => this.reload()
      });
  }

  private reload() {
    this.router.navigate([this.route.snapshot.url], { queryParamsHandling: "preserve" });
  }
}
