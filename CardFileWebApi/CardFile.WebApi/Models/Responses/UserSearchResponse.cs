﻿using CardFile.Application.Models;

namespace CardFile.WebApi.Models.Responses
{
    public class UserSearchResponse : IPaginatedResponse
    {
        public int Limit { get; set; }
        public int Offset { get; set; }
        public int TotalCount { get; set; }
        public IEnumerable<UserModel> Users { get; set; }
    }
}
