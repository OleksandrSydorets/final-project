import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Keyword } from 'src/app/models/keyword';
import { KeywordService } from 'src/app/services/keyword.service';

@Component({
  selector: 'app-keywords',
  templateUrl: './keywords.component.html',
  styleUrls: ['./keywords.component.css']
})
export class KeywordsComponent implements OnInit {
  keywords$!: Observable<Keyword[]>;
  selectedKeyword: Keyword | null = null;
  error: string | null = null;

  addForm!: FormGroup;
  editForm!: FormGroup;

  constructor(private keywordService: KeywordService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.keywords$ = this.keywordService.getAll();

    this.addForm = this.formBuilder.group({
      addKeywordName: ['', [
        Validators.required, 
        Validators.maxLength(20), 
        Validators.pattern(/^[a-zA-Z0-9_]+$/)
      ]]
    });

    this.editForm = this.formBuilder.group({
      editKeywordName: ['', [
        Validators.required, 
        Validators.maxLength(20), 
        Validators.pattern(/^[a-zA-Z0-9_]+$/)
      ]]
    });
  }

  onAddKeyword() {
    this.error = null;

    if (this.addForm.invalid) {
      this.addForm.markAllAsTouched();
      return;
    }

    const keywordNameInput = this.addForm.get('addKeywordName')!;
    const name = keywordNameInput.value as string;
    this.keywordService.post(name)
      .subscribe({ 
        next: () => {
          keywordNameInput.setValue('');
          keywordNameInput.markAsUntouched();

          this.keywords$ = this.keywordService.getAll()
        },
        error: (err: HttpErrorResponse) => {
          switch (err.status) {
            case 409: // keyword already exists
              this.error = err.error?.error;
              break;
            case 0:
              this.error = "Connection error";
              break;
            default:
              this.error = "Unknown error";
              break;
          }
        }
      });
  }

  onKeywordClick(keyword: Keyword) {
    this.error = null;
    
    this.selectedKeyword = keyword;
    this.editForm.patchValue({
      editKeywordName: keyword.name
    });
  }

  onEditKeyword() {
    this.error = null;
    
    const id = this.selectedKeyword!.id;
    const name = this.editForm.get('editKeywordName')!.value;
    
    this.keywordService.put(id, name)
      .subscribe({ 
        next: () => {
          this.selectedKeyword = null;
          
          this.keywords$ = this.keywordService.getAll()
        },
        error: (err: HttpErrorResponse) => {
          switch (err.status) {
            case 409: // keyword already exists
              this.error = err.error?.error;
              break;
            case 0:
              this.error = "Connection error";
              break;
            default:
              this.error = "Unknown error";
              break;
          }
        }
      });
  }

  onDeleteKeyword() {
    this.error = null;
    
    const id = this.selectedKeyword!.id;

    this.keywordService.delete(id)
      .subscribe(() => this.keywords$ = this.keywordService.getAll());

    this.selectedKeyword = null;
  }

  onCancelEdit() {
    this.error = null;
    
    this.selectedKeyword = null;
  }

  addKeywordNameValidCss() {
    const control = this.addForm.get('addKeywordName')!;

    return {
      'is-invalid': control.invalid && control.touched
    };
  }

  editKeywordNameValidCss() {
    const control = this.editForm.get('editKeywordName')!;

    return {
      'is-invalid': control.invalid && control.touched
    };
  }
}
