﻿namespace CardFile.WebApi.Models.Requests
{
    public class UserSearchQueryParameters
    {
        public string UserName { get; set; }
        public string Role { get; set; }
        public int Limit { get; set; } = 10;
        public int Offset { get; set; } = 0;

        public bool IsFilterEmpty()
        {
            return string.IsNullOrEmpty(UserName) && string.IsNullOrEmpty(Role);
        }
    }
}
