﻿using CardFile.Application.Models;
using CardFile.Application.Validators.ValidationRules;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Validators
{
    /// <summary>
    /// <see cref="FluentValidation"/> validator for <see cref="KeywordModel"/>.
    /// </summary>
    public class KeywordValidator : AbstractValidator<KeywordModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="KeywordValidator"/> class.
        /// </summary>
        public KeywordValidator()
        {
            RuleFor(k => k.Name)
                .Cascade(CascadeMode.Stop)
                .ValidKeywordName();
        }
    }
}
