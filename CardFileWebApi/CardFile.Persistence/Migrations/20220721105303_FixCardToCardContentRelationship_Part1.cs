﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CardFile.Persistence.Migrations
{
    public partial class FixCardToCardContentRelationship_Part1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_CardContents_ContentId",
                table: "Cards");

            migrationBuilder.DropTable(
                name: "CardContents");

            migrationBuilder.DropIndex(
                name: "IX_Cards_ContentId",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "ContentId",
                table: "Cards");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ContentId",
                table: "Cards",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "CardContents",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CardContents", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cards_ContentId",
                table: "Cards",
                column: "ContentId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_CardContents_ContentId",
                table: "Cards",
                column: "ContentId",
                principalTable: "CardContents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
