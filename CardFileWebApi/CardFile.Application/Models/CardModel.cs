﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Models
{
    /// <summary>
    /// Data transfer object that represents an index card.
    /// </summary>
    public class CardModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string AuthorId { get; set; }
        public string Author { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool Verified { get; set; }
        public ICollection<string> Keywords { get; set; }
    }
}
