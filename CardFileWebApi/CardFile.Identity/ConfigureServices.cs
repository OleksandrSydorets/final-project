﻿using CardFile.Application.RequiredInterfaces;
using CardFile.Identity.Entities;
using CardFile.Identity.Mapping;
using CardFile.Identity.Options;
using CardFile.Identity.Persistence;
using CardFile.Identity.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace CardFile.Identity
{
    /// <summary>
    /// Contains extension method for adding <see cref="Identity"/> services.
    /// </summary>
    public static class ConfigureServices
    {
        private const string ConnectionStringName = "IdentityDB";

        /// <summary>
        /// Adds <see cref="Identity"/> services
        /// </summary>
        /// <param name="services">the <see cref="IServiceCollection"/> to add services to</param>
        /// <param name="configuration">configuration</param>
        /// <returns>A reference to this instance after the operation has completed.</returns>
        public static IServiceCollection AddCardFileIdentity(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<CardFileIdentityDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString(ConnectionStringName));
            });

            services.AddIdentityCore<CardFileUser>()
                .AddRoles<CardFileRole>()
                .AddEntityFrameworkStores<CardFileIdentityDbContext>();

            services.Configure<JwtOptions>(configuration.GetSection(JwtOptions.Section));

            services.AddScoped<IIdentityService, IdentityService>();

            var jwtOptions = new JwtOptions();
            configuration.GetSection(JwtOptions.Section).Bind(jwtOptions);

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = jwtOptions.ValidIssuer,
                    ValidateAudience = true,
                    ValidAudience = jwtOptions.ValidAudience,
                    ValidateLifetime = true,
                    IssuerSigningKey = jwtOptions.GetSymmetricSecurityKey(),
                    ValidateIssuerSigningKey = true,
                };
            });

            services.AddAutoMapper(configuration => configuration.AddProfile<AutomapperProfile>());

            return services;
        }
    }
}
