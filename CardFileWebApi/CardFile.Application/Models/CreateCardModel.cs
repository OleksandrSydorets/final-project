﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Models
{
    /// <summary>
    /// Data transfer object that contains properties need to create an index card.
    /// </summary>
    public class CreateCardModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public IEnumerable<string> Keywords { get; set; }
    }
}
