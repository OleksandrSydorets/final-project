﻿using CardFile.Application.Exceptions;
using CardFile.Application.Models;

namespace CardFile.Application.Interfaces
{
    /// <summary>
    /// Defines functionality for adding, updating, deleting and retrieving index cards.
    /// </summary>
    public interface ICardService
    {
        /// <summary>
        /// Retrieves index cards.
        /// </summary>
        /// <param name="includeUnverified">
        /// If <see langword="true"/> retrieves all index cards, 
        /// if <see langword="false"/> only verified.
        /// </param>
        /// <param name="limit">max amount of cards to return</param>
        /// <param name="offset">amount of cards to skip</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved index cards.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// <paramref name="limit"/> is less then or equal to zero or
        /// <paramref name="offset"/> is less then zero.
        /// </exception>
        Task<IEnumerable<CardModel>> GetAllAsync(bool includeUnverified = false, int limit = 10, int offset = 0);

        /// <summary>
        /// Retrieves index cards by filter.
        /// </summary>
        /// <param name="filter">filter</param>
        /// <param name="limit">max amount of cards to return</param>
        /// <param name="offset">amount of cards to skip</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved index cards.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="filter"/> is <see langword="null"/>.</exception>
        /// <exception cref="FluentValidation.ValidationException"><paramref name="filter"/> invalid.</exception>
        /// <exception cref="ArgumentException">
        /// <paramref name="limit"/> is less then or equal to zero or
        /// <paramref name="offset"/> is less then zero.
        /// </exception>
        Task<IEnumerable<CardModel>> GetByFilterAsync(CardSearchFilterModel filter, int limit = 10, int offset = 0);

        /// <summary>
        /// Retrieves index card by id.
        /// </summary>
        /// <param name="id">card id</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved index card or <see langword="null"/>.
        /// </returns>
        Task<CardModel> GetByIdAsync(int id);

        /// <summary>
        /// Retrieves index card's content by card id.
        /// </summary>
        /// <param name="id">card id</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains card's content or <see langword="null"/>.
        /// </returns>
        Task<string> GetContentByIdAsync(int id);

        /// <summary>
        /// Adds new index card.
        /// </summary>
        /// <param name="card">index card to be added</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains newly created index card.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="card"/> is <see langword="null"/>.</exception>
        /// <exception cref="FluentValidation.ValidationException"><paramref name="card"/> is invalid.</exception>
        Task<CardModel> AddAsync(CreateCardModel card);

        /// <summary>
        /// Updates existing index card.
        /// </summary>
        /// <param name="card">object that represents new values</param>
        /// <returns>
        /// A task that represent the asynchronous operation.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="card"/> is <see langword="null"/>.</exception>
        /// <exception cref="FluentValidation.ValidationException"><paramref name="card"/> is invalid.</exception>
        /// <exception cref="EntityNotFoundException">Card with given id does not exist.</exception>
        Task UpdateAsync(UpdateCardModel card);

        /// <summary>
        /// Marks an index card as verified.
        /// </summary>
        /// <param name="id">card id</param>
        /// <returns>
        /// A task that represent the asynchronous operation.
        /// </returns>
        /// <exception cref="EntityNotFoundException">Card with given id does not exist.</exception>
        Task MarkVerified(int id);

        /// <summary>
        /// Deletes an index card.
        /// </summary>
        /// <param name="id">card id</param>
        /// <returns>
        /// A task that represent the asynchronous operation.
        /// </returns>
        /// <exception cref="EntityNotFoundException">Card with given id does not exist.</exception>
        Task DeleteByIdAsync(int id);

        /// <summary>
        /// Returns the number of index cards.
        /// </summary>
        /// <param name="countUnverified">Where to count unverified cards</param>
        /// <returns>The number of index cards.</returns>
        Task<int> Count(bool countUnverified = false);

        /// <summary>
        /// Returns the number of index cards after filtering. 
        /// </summary>
        /// <param name="filter">the filter</param>
        /// <returns>The number of index cards.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="filter"/> is <see langword="null"/>.</exception>
        /// <exception cref="FluentValidation.ValidationException"><paramref name="filter"/> invalid.</exception>
        Task<int> CountFiltered(CardSearchFilterModel filter);
    }
}
