﻿using CardFile.Domain.Entities;
using CardFile.Domain.Filters;

namespace CardFile.Domain.Repositories
{
    /// <summary>
    /// Defines functionality for persisting <see cref="Card"/> entities.
    /// </summary>
    public interface ICardRepository
    {
        /// <summary>
        /// Retrieves index cards.
        /// </summary>
        /// <param name="includeUnverified">
        /// If <see langword="true"/> retrieves all index cards, 
        /// if <see langword="false"/> only verified.
        /// </param>
        /// <param name="limit">max amount of cards to return</param>
        /// <param name="offset">amount of cards to skip</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved index cards.
        /// </returns>
        /// <exception cref="ArgumentException">
        /// <paramref name="limit"/> is less then or equal to zero or
        /// <paramref name="offset"/> is less then zero.
        /// </exception>
        Task<IEnumerable<Card>> GetAllAsync(bool includeUnverified = false, int limit = 10, int offset = 0);

        /// <summary>
        /// Retrieves index cards by filter.
        /// </summary>
        /// <param name="filter">filter</param>
        /// <param name="limit">max amount of cards to return</param>
        /// <param name="offset">amount of cards to skip</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved index cards.
        /// </returns>
        /// <exception cref="ArgumentNullException"><paramref name="filter"/> is <see langword="null"/>.</exception>
        /// <exception cref="ArgumentException">
        /// <paramref name="limit"/> is less then or equal to zero or
        /// <paramref name="offset"/> is less then zero.
        /// </exception>
        Task<IEnumerable<Card>> GetByFilterAsync(CardSearchFilter filter, int limit = 10, int offset = 0);

        /// <summary>
        /// Retrieves index card by id.
        /// </summary>
        /// <param name="id">card id</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved index card or <see langword="null"/>.
        /// </returns>
        Task<Card> GetByIdAsync(int id);

        /// <summary>
        /// Retrieves index card with it's content by id.
        /// </summary>
        /// <param name="id">card id</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved index card or <see langword="null"/>.
        /// </returns>
        Task<Card> GetByIdWithContentAsync(int id);

        /// <summary>
        /// Adds new index card.
        /// </summary>
        /// <param name="card">index card</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// </returns>
        Task AddAsync(Card card);

        /// <summary>
        /// Updates existing index card.
        /// </summary>
        /// <param name="card">index card</param>
        void Update(Card card);

        /// <summary>
        /// Deletes index card.
        /// </summary>
        /// <param name="card">card</param>
        void Delete(Card card);

        /// <summary>
        /// Deletes index card by id.
        /// </summary>
        /// <param name="id">card id</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// </returns>
        Task DelelteByIdAsync(int id);

        /// <summary>
        /// Returns the number of index cards.
        /// </summary>
        /// <param name="countUnverified">Where to count unverified cards</param>
        /// <returns>The number of index cards.</returns>
        Task<int> Count(bool countUnverified);

        /// <summary>
        /// Returns the number of index cards after filtering. 
        /// </summary>
        /// <param name="filter">the filter</param>
        /// <returns>The number of index cards.</returns>
        /// <exception cref="ArgumentNullException"><paramref name="filter"/> is <see langword="null"/>.</exception>
        Task<int> CountFiltered(CardSearchFilter filter);
    }
}
