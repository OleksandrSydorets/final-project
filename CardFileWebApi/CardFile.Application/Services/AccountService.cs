﻿using CardFile.Application.Interfaces;
using CardFile.Application.Models;
using CardFile.Application.RequiredInterfaces;
using CardFile.Application.Validators;
using CardFile.Domain;
using CardFile.Domain.Entities;
using CardFile.Domain.Repositories;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Services
{
    /// <summary>
    /// An <see cref="IAccountService"/> implementation.
    /// </summary>
    public class AccountService : IAccountService
    {
        private readonly IIdentityService _identityService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly AbstractValidator<UserSearchFilterModel> _filterValidator;
        private readonly IUserRepository _userRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="AccountService"/> class.
        /// </summary>
        /// <param name="identityService">Identity service that provides implementation details</param>
        /// <param name="unitOfWork">Unit of work</param>
        /// <param name="filterValidator">User search filter validtor</param>
        /// <exception cref="ArgumentNullException">
        ///     <paramref name="identityService"/> or <paramref name="unitOfWork"/> is null.
        /// </exception>
        public AccountService(IIdentityService identityService, IUnitOfWork unitOfWork, AbstractValidator<UserSearchFilterModel> filterValidator)
        {
            _identityService = identityService ?? throw new ArgumentNullException(nameof(identityService));
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _filterValidator = filterValidator ?? throw new ArgumentNullException(nameof(filterValidator));
            _userRepository = _unitOfWork.UserRepository;
        }

        /// <inheritdoc/>
        public Task ChangePasswordAsync(string userName, string currentPassword, string newPassword)
        {
            return _identityService.ChangePasswordAsync(userName, currentPassword, newPassword);
        }

        /// <inheritdoc/>
        public Task<string> LoginAsync(string userName, string password)
        {
            return _identityService.LoginAsync(userName, password);
        }

        /// <inheritdoc/>
        public async Task<string> RegisterAsync(string userName, string password)
        {
            var result = await _identityService.RegisterAsync(userName, password);

            var user = new User
            {
                Id = result.userId,
                DisplayName = userName
            };

            await _userRepository.AddAsync(user);
            await _unitOfWork.SaveAsync();

            return result.token;
        }

        /// <inheritdoc/>
        public Task ChangeRoleAsync(string userId, string role)
        {
            return _identityService.ChangeRoleAsync(userId, role);
        }

        /// <inheritdoc/>
        public Task<IEnumerable<UserModel>> GetUsersAsync(int limit = 10, int offset = 0, UserSearchFilterModel filter = null)
        {
            if (filter is not null)
            {
                _filterValidator.ValidateAndThrow(filter);
            }

            return _identityService.GetUsersAsync(limit, offset, filter);
        }

        /// <inheritdoc/>
        public Task<int> GetUserCountAsync(UserSearchFilterModel filter = null)
        {
            if (filter is not null)
            {
                _filterValidator.ValidateAndThrow(filter);
            }

            return _identityService.GetUserCountAsync(filter);
        }

        /// <inheritdoc/>
        public Task<UserModel> GetUserByIdAsync(string id)
        {
            return _identityService.GetUserByIdAsync(id);
        }
    }
}
