import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { CardService } from 'src/app/services/card.service';
import { KeywordService } from 'src/app/services/keyword.service';

@Component({
  selector: 'app-create-card',
  templateUrl: './create-card.component.html',
  styleUrls: ['./create-card.component.css']
})
export class CreateCardComponent implements OnInit {
  error: string | null = null;
  keywords: string[] = [];

  createForm!: FormGroup;
  keywordsControls!: FormArray;

  constructor(
    private cardService: CardService, 
    private keywordService: KeywordService,
    private formBuilder: FormBuilder,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.keywordsControls = this.formBuilder.array([]);
    
    this.createForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.maxLength(200)]],
      content: ['', Validators.required],
      keywords: this.keywordsControls
    });

    this.getKeywords()
      .subscribe(keywords => keywords
        .forEach(keyword => {
          this.keywordsControls.push(this.formBuilder.control(false));
          this.keywords.push(keyword);
        }))
  }

  getKeywords(): Observable<string[]> {
    return this.keywordService.getNames();
    //return of([...Array(30).keys()].map(i => `keyword ${i}`));
  }

  get title(): string {
    return this.createForm.get('title')!.value;
  }

  get content(): string {
    return  this.createForm.get('content')!.value;
  }

  get selectedKeywordsString(): string {
    return this.selectedKeywords.join(', ');
  }

  get selectedKeywords(): string[] {
    return this.keywordsControls.controls
      .map((c, i) => c.value ? this.keywords[i] : '')
      .filter(k => k);
  }

  controlValidCss(controlName: string) {
    const control = this.createForm.get(controlName);

    if (!control) {
      return { };
    }

    return {
      'is-invalid': control.invalid && control.touched,
    }
  }

  onSubmit() {
    if (this.createForm.invalid) {
      this.createForm.markAllAsTouched();
      return;
    }

    this.createForm.disable();
    this.error = null;

    this.cardService.postCard(this.title, this.content, this.selectedKeywords)
      .subscribe({
        next: card => this.router.navigate(['/cards', card.id]),
        error: (err: HttpErrorResponse) => {
          switch (err.status) {
            case 400:
              this.error = err.error?.title;
              break;
            case 0:
              this.error = "Connection error";
              break;
            default:
              this.error = "Unknown error";
              break;
          }
        }
      });
  }
}
