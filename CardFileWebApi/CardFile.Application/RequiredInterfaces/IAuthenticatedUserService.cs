﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.RequiredInterfaces
{
    /// <summary>
    /// Defines functionality for accessing current authenticated user's information.
    /// </summary>
    public interface IAuthenticatedUserService
    {
        /// <summary>
        /// Gets current user's id.
        /// </summary>
        string UserId { get; }
    }
}
