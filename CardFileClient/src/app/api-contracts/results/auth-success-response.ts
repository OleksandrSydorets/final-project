export interface AuthSuccessResponse {
    token: string
}