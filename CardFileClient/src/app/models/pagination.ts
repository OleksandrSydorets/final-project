import { Params, UrlSegment } from "@angular/router";

export class Page {
  isSkipedRange: false = false;

  page: number;
  active: boolean;
  url: UrlSegment[];
  queryParams: Params;

  constructor(page: number, active: boolean, url: UrlSegment[], queryParams: Params) {
    this.page = page;
    this.active = active;
    this.url = url;
    this.queryParams = queryParams;
  }
}

export class SkipedPageRange {
  isSkipedRange: true = true;
}

export type PaginationItem = Page | SkipedPageRange;

export class PrevNextPageEnabled {
  disabled: false = false;
  url: UrlSegment[];
  queryParams: Params;
  onClick: () => void;

  constructor(url: UrlSegment[], queryParams: Params, onClick: () => void) {
    this.url = url;
    this.queryParams = queryParams;
    this.onClick = onClick;
  }
}

export class PrevNextPageDisabled {
  disabled: true = true;
  url: undefined;
  queryParams: undefined;
  onClick = () => { };
}

export type PrevNextPage = PrevNextPageEnabled | PrevNextPageDisabled;