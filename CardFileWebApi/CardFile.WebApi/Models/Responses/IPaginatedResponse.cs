﻿namespace CardFile.WebApi.Models.Responses
{
    public interface IPaginatedResponse
    {
        int Limit { get; set; }
        int Offset { get; set; }
        int TotalCount { get; set; }
    }
}