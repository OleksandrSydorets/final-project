﻿namespace CardFile.Application.Models
{
    /// <summary>
    /// Data transfer object that represents criteria by which index cards can be filtered.
    /// </summary>
    public class CardSearchFilterModel
    {
        public string Title { get; set; }
        public string AuthorId { get; set; }
        public string Author { get; set; }
        public bool IncludeUnverified { get; set; } = false;
        public DateTime? After { get; set; }
        public DateTime? Before { get; set; }
        public IEnumerable<string> Keywords { get; set; }

        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(Title)
                && string.IsNullOrEmpty(AuthorId)
                && string.IsNullOrEmpty(Author)
                && !IncludeUnverified
                && After is null
                && Before is null
                && (Keywords is null || !Keywords.Any());
        }
    }
}
