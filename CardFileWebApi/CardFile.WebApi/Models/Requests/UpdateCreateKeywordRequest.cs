﻿using System.ComponentModel.DataAnnotations;

namespace CardFile.WebApi.Models.Requests
{
    public class UpdateKeywordRequest
    {
        [Required]
        public string Name { get; set; }
    }
}
