﻿using AutoMapper;
using CardFile.Application.Models;
using CardFile.Domain.Entities;
using CardFile.Domain.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Mapping
{
    internal class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Keyword, KeywordModel>();

            CreateMap<Card, CardModel>()
                .ForMember(d => d.Author, s => s.MapFrom(s => s.Author.DisplayName))
                .ForMember(d => d.Keywords, s => s.MapFrom(s => s.Keywords.Select(k => k.Name)));

            CreateMap<CardSearchFilterModel, CardSearchFilter>();
        }
    }
}
