﻿using CardFile.Domain.Entities;

namespace CardFile.Domain.Repositories
{
    /// <summary>
    /// Defines functionality for persisting <see cref="Keyword"/> entities.
    /// </summary>
    public interface IKeywordRepository
    {
        /// <summary>
        /// Retrieves keywords.
        /// </summary>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved keywords.
        /// </returns>
        Task<IEnumerable<Keyword>> GetAllAsync();

        /// <summary>
        /// Retrieves keywords with provided names.
        /// </summary>
        /// <param name="names">keywords names</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved keywords.
        /// </returns>
        Task<IEnumerable<Keyword>> GetByNamesAsync(IEnumerable<string> names);

        /// <summary>
        /// Retrieves keyword by id.
        /// </summary>
        /// <param name="id">keyword id</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved keyword.
        /// </returns>
        Task<Keyword> GetByIdAsync(int id);

        /// <summary>
        /// Retrieves keyword by name.
        /// </summary>
        /// <param name="name">keyword name</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved keyword.
        /// </returns>
        Task<Keyword> GetByNameAsync(string name);

        /// <summary>
        /// Adds new keyword.
        /// </summary>
        /// <param name="keyword">keyword</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// </returns>
        Task AddAsync(Keyword keyword);

        /// <summary>
        /// Updates existing keyword.
        /// </summary>
        /// <param name="keyword">keyword</param>
        void Update(Keyword keyword);

        /// <summary>
        /// Deletes keyword.
        /// </summary>
        /// <param name="keyword">keyword</param>
        void Delete(Keyword keyword);

        /// <summary>
        /// Deletes keyword by id
        /// </summary>
        /// <param name="id">keyword id</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// </returns>
        Task DelelteByIdAsync(int id);
    }
}
