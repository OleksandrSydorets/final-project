﻿using System.ComponentModel.DataAnnotations;

namespace CardFile.WebApi.Models.Requests
{
    public class ChangeRoleRequest
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public string Role { get; set; }
    }
}
