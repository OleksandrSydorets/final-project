﻿using CardFile.Application.Models;
using CardFile.Application.Validators.ValidationRules;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Validators
{
    /// <summary>
    /// <see cref="FluentValidation"/> validator for <see cref="CreateKeywordModel"/>.
    /// </summary>
    public class CreateKeywordValidator : AbstractValidator<CreateKeywordModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CreateKeywordValidator"/> class.
        /// </summary>
        public CreateKeywordValidator()
        {
            RuleFor(k => k.Name)
                .Cascade(CascadeMode.Stop)
                .ValidKeywordName();
        }
    }
}
