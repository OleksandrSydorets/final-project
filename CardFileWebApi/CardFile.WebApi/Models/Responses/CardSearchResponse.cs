﻿using CardFile.Application.Models;

namespace CardFile.WebApi.Models.Responses
{
    public class CardSearchResponse : IPaginatedResponse
    {
        public int Limit { get; set; }
        public int Offset { get; set; }
        public int TotalCount { get; set; }
        public IEnumerable<CardModel> Cards { get; set; }
    }
}
