﻿using System.ComponentModel.DataAnnotations;

namespace CardFile.WebApi.Models.Requests
{
    public class UpdateCardRequest
    {
        [Required]
        public string Title { get; set; }

        public IEnumerable<string> Keywords { get; set; }
    }
}
