import { AbstractControl, ValidationErrors } from "@angular/forms";

const lowerCaseExp = /[a-z]/;
const upperCaseExp = /[A-Z]/;
const digitExp = /\d/;
const nonAlphaNumericExp = /\W/;

export class ControlValidators {
  static passwordsMatch(control: AbstractControl): ValidationErrors | null {
    const password = control.get('password')?.value;
    const confirmPassword = control.get('confirmPassword')?.value;

    console.log(JSON.stringify(control.get('password')?.errors));

    return password === confirmPassword ? null : { passwordsMatch: true };
  }

  static haslowerCaseLetter(control: AbstractControl): ValidationErrors | null {
    const value: string | null = control.value;
    return value && lowerCaseExp.test(value) ? null : { haslowerCaseLetter: true };
  }

  static hasUpperCaseLetter(control: AbstractControl): ValidationErrors | null {
    const value: string | null = control.value;
    return value && upperCaseExp.test(value) ? null : { hasUpperCaseLetter: true };
  }

  static hasDigit(control: AbstractControl): ValidationErrors | null {
    const value: string | null = control.value;
    return value && digitExp.test(value) ? null : { hasDigit: true };
  }

  static hasNonAlphaNumeric(control: AbstractControl): ValidationErrors | null {
    const value: string | null = control.value;
    return value && nonAlphaNumericExp.test(value) ? null : { hasNonAlphaNumeric: true };
  }

  static notEqualTo(invalidValue: any) {
    return (control: AbstractControl): ValidationErrors | null => {
      return control.value !== invalidValue ? null : { notEqualTo: true };
    }
  }
}