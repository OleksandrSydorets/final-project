﻿namespace CardFile.Application.Models
{
    /// <summary>
    /// Contains roles that application users can have.
    /// </summary>
    public static class UserRoles
    {
        public const string User = "User";
        public const string Moderator = "Moderator";
        public const string Admin = "Admin";

        public const string AdminOrModerator = $"{Admin},{Moderator}";

        public static readonly IEnumerable<string> Roles = new string[] { User, Moderator, Admin };
    }
}
