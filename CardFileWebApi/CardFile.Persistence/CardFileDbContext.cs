﻿using CardFile.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardFile.Persistence.Entities;

namespace CardFile.Persistence
{
    /// <summary>
    /// EF Core DbContext used to persist domain entities in a database.
    /// </summary>
    public class CardFileDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CardFileDbContext"/> class using the specified options.
        /// </summary>
        /// <param name="options">The options for this context</param>
        public CardFileDbContext(DbContextOptions<CardFileDbContext> options) : base(options) { }

        public DbSet<Card> Cards { get; set; }
        public DbSet<CardContent> CardContents { get; set; }
        public DbSet<Keyword> Keywords { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Card>(b =>
            {
                b.Property(m => m.Title)
                    .IsRequired()
                    .HasMaxLength(400);

                b.Property(m => m.AuthorId)
                    .IsRequired()
                    .HasMaxLength(450);

                b.HasIndex(m => m.CreatedAt);
            });

            modelBuilder.Entity<CardContent>(b =>
            {
                b.Property(m => m.Content)
                    .IsRequired();

                b.HasKey(m => m.CardId);

                b.HasOne<Card>()
                    .WithOne(m => m.Content)
                    .HasForeignKey<CardContent>(m => m.CardId)
                    .OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<Keyword>(b =>
            {
                b.Property(m => m.Name)
                    .HasMaxLength(20)
                    .IsRequired();

                b.HasIndex(m => m.Name)
                    .IsUnique();
            });

            modelBuilder.Entity<User>(b =>
            {
                b.Property(m => m.Id)
                    .HasMaxLength(450);

                b.Property(m => m.DisplayName)
                    .HasMaxLength(256)
                    .IsRequired();

                b.Property(m => m.FirsName)
                    .HasMaxLength(256);

                b.Property(m => m.LastName)
                    .HasMaxLength(256);
            });

            modelBuilder.Entity<Card>()
                .HasMany(c => c.Keywords)
                .WithMany(k => k.Cards)
                .UsingEntity<CardKeywords>(
                    j => j
                        .HasOne(j => j.Keyword)
                        .WithMany()
                        .HasForeignKey(j => j.KeywordId),
                    j => j
                        .HasOne(j => j.Card)
                        .WithMany()
                        .HasForeignKey(j => j.CardId)
                    );

            modelBuilder.Entity<User>()
                .HasData(
                    new User
                    {
                        Id = "6f193093-c9e1-42f4-8b93-1bd154ca9c0b",
                        DisplayName = "Admin",
                    });
        }
    }
}
