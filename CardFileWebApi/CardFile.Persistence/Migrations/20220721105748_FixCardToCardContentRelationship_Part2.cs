﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CardFile.Persistence.Migrations
{
    public partial class FixCardToCardContentRelationship_Part2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CardContents",
                columns: table => new
                {
                    CardId = table.Column<int>(type: "int", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CardContents", x => x.CardId);
                    table.ForeignKey(
                        name: "FK_CardContents_Cards_CardId",
                        column: x => x.CardId,
                        principalTable: "Cards",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CardContents");
        }
    }
}
