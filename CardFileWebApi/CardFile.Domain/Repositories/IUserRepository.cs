﻿using CardFile.Domain.Entities;

namespace CardFile.Domain.Repositories
{
    /// <summary>
    /// Defines functionality for persisting <see cref="User"/> entities.
    /// </summary>
    public interface IUserRepository
    {
        /// <summary>
        /// Retrieves user.
        /// </summary>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved users.
        /// </returns>
        Task<IEnumerable<User>> GetAllAsync();

        /// <summary>
        /// Retrieves user.
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved user.
        /// </returns>
        Task<User> GetByIdAsync(int id);

        /// <summary>
        /// Adds new user.
        /// </summary>
        /// <param name="user">user</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// </returns>
        Task AddAsync(User user);

        /// <summary>
        /// Updates existing user.
        /// </summary>
        /// <param name="user">user</param>
        void Update(User user);

        /// <summary>
        /// Deletes user.
        /// </summary>
        /// <param name="user">user</param>
        void Delete(User user);

        /// <summary>
        /// Deletes user by id.
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// </returns>
        Task DelelteByIdAsync(int id);
    }
}
