import { User } from "src/app/models/user";
import { PaginatedResponse } from "./paginated-response";

export interface UserSearchResponse extends PaginatedResponse {
    users: User[];
}