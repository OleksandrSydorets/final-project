﻿using CardFile.Domain.Entities;
using CardFile.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Domain
{
    /// <summary>
    /// Represents a session with the database and can be used to query and save entities.
    /// </summary>
    public interface IUnitOfWork
    {
        /// <summary>
        /// Gets <see cref="Card"/> repository.
        /// </summary>
        public ICardRepository CardRepository { get; }

        /// <summary>
        /// Gets <see cref="Keyword"/> repository.
        /// </summary>
        public IKeywordRepository KeywordRepository { get; }

        /// <summary>
        /// Gets <see cref="User"/> repository.
        /// </summary>
        public IUserRepository UserRepository { get; }

        /// <summary>
        /// Saves all changes to the database.
        /// </summary>
        /// <returns></returns>
        Task SaveAsync();
    }
}
