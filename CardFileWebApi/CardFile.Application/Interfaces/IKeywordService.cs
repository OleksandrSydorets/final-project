﻿using CardFile.Application.Exceptions;
using CardFile.Application.Models;
using FluentValidation;

namespace CardFile.Application.Interfaces
{
    /// <summary>
    /// Defines functionality for adding, updating, deleting and retrieving keywords.
    /// </summary>
    public interface IKeywordService
    {
        /// <summary>
        /// Retrieves keywords.
        /// </summary>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved keywords.
        /// </returns>
        Task<IEnumerable<KeywordModel>> GetAllAsync();

        /// <summary>
        /// Retrieves keyword by id.
        /// </summary>
        /// <param name="id">keyword id</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains retrieved keyword or <see langword="null"/>.
        /// </returns>
        Task<KeywordModel> GetByIdAsync(int id);

        /// <summary>
        /// Adds new keyword.
        /// </summary>
        /// <param name="keyword">keyword</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// The task result contains newly created keywords.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="keyword"/> is <see langword="null"/>
        /// </exception>
        /// <exception cref="ValidationException">
        /// <paramref name="keyword"/> is invalid.
        /// </exception>
        /// <exception cref="KeywordAlreadyExistsException">
        /// Keyword with given name already exists.
        /// </exception>
        Task<KeywordModel> AddAsync(CreateKeywordModel keyword);

        /// <summary>
        /// Updates existing keyword.
        /// </summary>
        /// <param name="keyword">keyword</param>
        /// <returns>
        /// A task that represent the asynchronous operation.
        /// </returns>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="keyword"/> is <see langword="null"/>
        /// </exception>
        /// <exception cref="ValidationException">
        /// <paramref name="keyword"/> is invalid.
        /// </exception>
        /// <exception cref="KeywordAlreadyExistsException">
        /// Keyword with given name already exists.
        /// </exception>
        /// <exception cref="EntityNotFoundException">
        /// Keyword with given id doesn't exist.
        /// </exception>
        Task UpdateAsync(KeywordModel keyword);

        /// <summary>
        /// Deletes keyword.
        /// </summary>
        /// <param name="id">keyword id</param>
        /// <returns>
        /// A task that represent the asynchronous operation. 
        /// </returns>
        /// <exception cref="EntityNotFoundException">
        /// Keyword with given id doesn't exist.
        /// </exception>
        Task DeleteByIdAsync(int id);
    }
}
