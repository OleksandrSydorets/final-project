export class RegistrationResult {
    successful: boolean;
    errors?: string[];

    constructor(successful: true);
    constructor(successful: false, errors: string[])
    constructor(successful: boolean, errors?: string[]) {
        this.successful = successful;
        
        if (!successful) {
            this.errors = errors;
        }
    }
}
