import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TokenInterceptor } from './interceptors/token.interceptor';
import { RegistrationComponent } from './components/registration/registration.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { LoginMenuComponent } from './components/login-menu/login-menu.component';
import { CardsComponent } from './components/cards/cards.component';
import { CardItemComponent } from './components/card-item/card-item.component';
import { CardComponent } from './components/card/card.component';
import { CreateCardComponent } from './components/create-card/create-card.component';
import { EditCardComponent } from './components/edit-card/edit-card.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { PageForbiddenComponent } from './components/page-forbidden/page-forbidden.component';
import { KeywordsComponent } from './components/keywords/keywords.component';
import { CardSearchResultComponent } from './components/card-search-result/card-search-result.component';
import { SearchResultNavigationComponent } from './components/search-result-navigation/search-result-navigation.component';
import { UsersComponent } from './components/users/users.component';
import { UserSearchResultComponent } from './components/user-search-result/user-search-result.component';
import { UserItemComponent } from './components/user-item/user-item.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    NavMenuComponent,
    LoginMenuComponent,
    CardsComponent,
    CardItemComponent,
    CardComponent,
    CreateCardComponent,
    EditCardComponent,
    PageNotFoundComponent,
    PageForbiddenComponent,
    KeywordsComponent,
    CardSearchResultComponent,
    SearchResultNavigationComponent,
    UsersComponent,
    UserSearchResultComponent,
    UserItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
