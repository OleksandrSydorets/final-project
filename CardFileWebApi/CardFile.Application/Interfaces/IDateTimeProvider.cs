﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Interfaces
{
    /// <summary>
    /// Defines functionality for providing current <see cref="DateTime"/>.
    /// </summary>
    public interface IDateTimeProvider
    {
        /// <summary>
        /// Gets current UTC <see cref="DateTime"/>.
        /// </summary>
        DateTime UtcNow { get; }
    }
}
