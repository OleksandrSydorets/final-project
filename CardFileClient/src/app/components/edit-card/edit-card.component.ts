import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { map, Observable } from 'rxjs';
import { CardService } from 'src/app/services/card.service';
import { KeywordService } from 'src/app/services/keyword.service';

@Component({
  selector: 'app-edit-card',
  templateUrl: './edit-card.component.html',
  styleUrls: ['./edit-card.component.css']
})
export class EditCardComponent implements OnInit {
  error: string | null = null;
  cardId!: number;
  content$!: Observable<string>;
  keywords: string[] = [];

  editForm!: FormGroup;
  keywordsControls!: FormArray;

  constructor(
    private cardService: CardService, 
    private keywordService: KeywordService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.cardId = parseInt(this.route.snapshot.params['id']);

    this.keywordsControls = this.formBuilder.array([]);
    
    this.editForm = this.formBuilder.group({
      title: ['', [Validators.required, Validators.maxLength(200)]],
      // content: ['', Validators.required],
      keywords: this.keywordsControls
    });

    this.getKeywords()
      .subscribe(keywords => keywords
        .forEach(keyword => {
          this.keywordsControls.push(this.formBuilder.control(false));
          this.keywords.push(keyword);
        }));
    
    this.content$ = this.cardService.getContentById(this.cardId);
    this.cardService.getById(this.cardId)
      .subscribe(card => {
        this.editForm.patchValue({
          title: card.title,
          keywords: this.keywords.map(k => card.keywords.includes(k))
        });
      });
  }

  get title(): string {
    return this.editForm.get('title')!.value;
  }

  get selectedKeywordsString(): string {
    return this.selectedKeywords.join(', ');
  }

  get selectedKeywords(): string[] {
    return this.keywordsControls.controls
      .map((c, i) => c.value ? this.keywords[i] : '')
      .filter(k => k);
  }

  getKeywords(): Observable<string[]> {
    return this.keywordService.getNames();
  }

  controlValidCss(controlName: string) {
    const control = this.editForm.get(controlName);

    if (!control) {
      return { };
    }

    return {
      'is-invalid': control.invalid && control.touched,
    }
  }

  onSubmit() {
    if (this.editForm.invalid) {
      this.editForm.markAllAsTouched();
      return;
    }

    this.editForm.disable();
    this.error = null;

    this.cardService.putCard(this.cardId, this.title, this.selectedKeywords)
      .subscribe({
        next: () => this.router.navigate(['/cards', this.cardId]),
        error: (err: HttpErrorResponse) => {
          switch (err.status) {
            case 400:
              this.error = err.error?.title;
              break;
            case 0:
              this.error = "Connection error";
              break;
            default:
              this.error = "Unknown error";
              break;
          }
        }
      });
  }

}
