import { Card } from "src/app/models/card";
import { PaginatedResponse } from "./paginated-response";

export interface CardSearchResponse extends PaginatedResponse {
    cards: Card[];
}