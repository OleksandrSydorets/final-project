﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Models
{
    /// <summary>
    /// Data transfer object that represents a keyword used to categorize index cards.
    /// </summary>
    public class KeywordModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
