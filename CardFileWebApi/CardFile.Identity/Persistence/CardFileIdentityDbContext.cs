﻿using CardFile.Identity.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

using ApplicationRoles = CardFile.Application.Models.UserRoles;

namespace CardFile.Identity.Persistence
{
    /// <summary>
    /// EF Core DbContext used to persist identity entities in a database.
    /// </summary>
    public class CardFileIdentityDbContext : IdentityDbContext<CardFileUser, CardFileRole, string>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CardFileIdentityDbContext"/> class using the specified options.
        /// </summary>
        /// <param name="options">The options for this context</param>
        public CardFileIdentityDbContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<CardFileUser>(b =>
            {
                b.HasMany(m => m.Roles)
                    .WithMany(m => m.Users)
                    .UsingEntity<IdentityUserRole<string>>();
            });

            builder.Entity<CardFileRole>()
                .HasData(
                    new CardFileRole
                    {
                        Id = "fe66f269-60ef-4290-a967-caf89c3adeac",
                        Name = ApplicationRoles.Admin,
                        NormalizedName = ApplicationRoles.Admin.ToUpper(),
                        ConcurrencyStamp = "a216f869-e5e4-471a-a5ea-f47dd8c88a56"
                    },
                    new CardFileRole
                    {
                        Id = "b7cc07ad-7314-446f-aebb-5d8b923adcb1",
                        Name = ApplicationRoles.User,
                        NormalizedName = ApplicationRoles.User.ToUpper(),
                        ConcurrencyStamp = "fd8aea0e-b181-4934-bbc5-f9725ceb2b16"
                    },
                    new CardFileRole
                    {
                        Id = "a7d31612-67ec-4f56-ba97-c39e2ef77ea5",
                        Name = ApplicationRoles.Moderator,
                        NormalizedName = ApplicationRoles.Moderator.ToUpper(),
                        ConcurrencyStamp = "27d9f92f-ce97-4bf2-9ab6-1ce174b50489"
                    });

            builder.Entity<CardFileUser>()
                .HasData(
                    new CardFileUser
                    {
                        Id = "6f193093-c9e1-42f4-8b93-1bd154ca9c0b",
                        UserName = "Admin",
                        NormalizedUserName = "ADMIN",
                        EmailConfirmed = false,
                        PasswordHash = "AQAAAAEAACcQAAAAEBj2l1CS0A1TpZSvw6C8p7CHl56gIMzlsf4cy1UGNa4WjTbT8jZvqRKK+5FyqKWorQ==",
                        SecurityStamp = "2EY6ECVKEALZD5JTNTB2XT663OMWMPDV",
                        ConcurrencyStamp = "8b5a93c4-9a6c-4859-8027-e7003ce6aede",
                        PhoneNumberConfirmed = false,
                        TwoFactorEnabled = false,
                        LockoutEnabled = true,
                        AccessFailedCount = 0,
                    });

            builder.Entity<IdentityUserRole<string>>()
                .HasData(
                    new IdentityUserRole<string>
                    {
                        UserId = "6f193093-c9e1-42f4-8b93-1bd154ca9c0b",
                        RoleId = "fe66f269-60ef-4290-a967-caf89c3adeac"
                    });
        }
    }
}
