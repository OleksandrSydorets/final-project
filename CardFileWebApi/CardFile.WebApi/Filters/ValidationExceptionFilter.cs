﻿using CardFile.WebApi.Models.Responses;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;

namespace CardFile.WebApi.Filters
{
    /// <summary>
    /// Catches <see cref="ValidationException"/> and sets Result to 400 <see cref="ValidationErrorResponse"/>
    /// containing validation errors.
    /// </summary>
    public class ValidationExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (context.Exception is not ValidationException)
            {
                return;
            }

            var exception = context.Exception as ValidationException;

            var errors = exception.Errors
                .GroupBy(x => x.PropertyName, x => x.ErrorMessage)
                .ToDictionary(x => x.Key, x => x.Select(e => e));

            var response = new ValidationErrorResponse(errors);

            context.Result = new JsonResult(response)
            {
                StatusCode = 400
            };

            context.ExceptionHandled = true;
        }
    }
}
