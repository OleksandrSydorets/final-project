import { environment } from "src/environments/environment";

const apiUrl = environment.apiUrl;

const accounthUrl = path(apiUrl, "api/account");
const weatherUrl = path(apiUrl, "weatherforecast");
const cardsUrl = path(apiUrl, "api/cards");
const keywordsUrl = path(apiUrl, "api/keywords")

export const ApiPaths : ApiPaths = {
    Account: {
        Login: path(accounthUrl, "login"),
        Register: path(accounthUrl, "register"),
        ChangePassword: path(accounthUrl, "change-password"),
        ChangeRole: path(accounthUrl, "change-role"),
        Users: {
            Base: path(accounthUrl, "users"),
            byId(id: number) {
                return path(accounthUrl, "users", id.toString());
            },
        }
    },
    WeatherForecast: {
        Base: weatherUrl,
        UserOnly: path(weatherUrl, "user"),
        AdminOnly: path(weatherUrl, "admin"),
    },
    Cards: {
        Base: cardsUrl,
        byId(id: number) {
            return path(cardsUrl, id.toString());
        },
        contentById(id: number) {
            return path(cardsUrl, id.toString(), 'content');
        },
        verifyById(id: number) {
            return path(cardsUrl, id.toString(), 'mark-verified')
        }
    },
    Keywords: {
        Base: keywordsUrl,
        byId(id: number) {
            return path(keywordsUrl, id.toString());
        },
    },
}

interface ApiPaths {
    readonly Account: {
        readonly Login: string,
        readonly Register: string,
        readonly ChangePassword: string,
        readonly ChangeRole: string,
        readonly Users: {
            readonly Base: string,
            byId(id: number): string,
        },
    },
    readonly WeatherForecast: {
        readonly Base: string,
        readonly UserOnly: string,
        readonly AdminOnly: string,
    },
    readonly Cards: {
        readonly Base: string,
        byId(id: number): string,
        contentById(id: number): string,
        verifyById(id: number): string,
    },
    readonly Keywords: {
        readonly Base: string,
        byId(id: number): string,
    },
}

function path(...parts: string[]): string {
    return parts.join("/");
}