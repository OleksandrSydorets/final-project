import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UserSearchResponse } from 'src/app/api-contracts/results/user-search-response';

@Component({
  selector: 'app-user-search-result',
  templateUrl: './user-search-result.component.html',
  styleUrls: ['./user-search-result.component.css']
})
export class UserSearchResultComponent implements OnInit {
  @Input() searchResult: UserSearchResponse | null = null;
  @Output() changePageClick = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  onChangePageClick(page: number) {
    this.changePageClick.emit(page);
  }
}
