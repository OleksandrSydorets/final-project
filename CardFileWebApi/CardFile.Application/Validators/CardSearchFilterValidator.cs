﻿using CardFile.Application.Models;
using CardFile.Application.Validators.ValidationRules;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Validators
{
    /// <summary>
    /// <see cref="FluentValidation"/> validator for <see cref="CardSearchFilterModel"/>.
    /// </summary>
    public class CardSearchFilterValidator : AbstractValidator<CardSearchFilterModel>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CardSearchFilterValidator"/> class.
        /// </summary>
        public CardSearchFilterValidator()
        {
            RuleFor(m => m.Keywords)
                .ForEach(k => k.ValidKeywordName());

            RuleFor(m => m.Author)
                .Must(BeNullOrEmptyIfAuthorIdNotNullOrEmpty);
        }

        private bool BeNullOrEmptyIfAuthorIdNotNullOrEmpty(CardSearchFilterModel model, string author)
        {
            return string.IsNullOrEmpty(model.AuthorId) || string.IsNullOrEmpty(author);
        }
    }
}
