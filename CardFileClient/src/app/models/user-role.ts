export class UserRole {
	static readonly User = 'User';
	static readonly Moderator = 'Moderator';
	static readonly Admin = 'Admin';

  static get Roles(): string[] {
    return [
      this.User, 
      this.Moderator, 
      this.Admin
    ];
  }

	static isModeratorOrAdmin(role: string | null): boolean {
    return role === this.Moderator || role === this.Admin;
	}
}