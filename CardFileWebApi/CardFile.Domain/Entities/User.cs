﻿namespace CardFile.Domain.Entities
{
    /// <summary>
    /// Represent a user that can create index cards.
    /// </summary>
    public class User
    {
        public string Id { get; set; }
        public string DisplayName { get; set; }
        public string FirsName { get; set; }
        public string LastName { get; set; }

        public ICollection<Card> Cards { get; set; }
    }
}
