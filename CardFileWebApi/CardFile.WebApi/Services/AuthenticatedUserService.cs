﻿using CardFile.Application.RequiredInterfaces;
using System.Security.Claims;

namespace CardFile.WebApi.Services
{
    /// <summary>
    /// An <see cref="IAuthenticatedUserService"/> implementation using <see cref="IHttpContextAccessor"/>.
    /// </summary>
    public class AuthenticatedUserService : IAuthenticatedUserService
    {
        private readonly HttpContext _context;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticatedUserService"/> class.
        /// </summary>
        /// <param name="contextAccessor">Http context accessor</param>
        public AuthenticatedUserService(IHttpContextAccessor contextAccessor)
        {
            _context = contextAccessor.HttpContext;
        }

        /// <inheritdoc/>
        public string UserId => _context?.User?.FindFirstValue("id");
    }
}
