import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ChangeRoleRequest } from '../api-contracts/requests/change-role-request';
import { UserSearchResponse } from '../api-contracts/results/user-search-response';
import { ApiPaths } from '../api-paths';
import { UserFilter } from '../models/user-filter';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  getByFilter(filter: UserFilter, pageNumber: number = 1, pageSize: number = 10): Observable<UserSearchResponse> {
    const offset = (pageNumber - 1) * pageSize;

    const params = this.paramsFromFilter(filter)
      .set('limit', pageSize)
      .set('offset', offset);
    
    return this.httpClient.get<UserSearchResponse>(ApiPaths.Account.Users.Base, { params: params });
  }

  changeRole(userId: string, role: string) {
    const request = new ChangeRoleRequest(userId, role);
    return this.httpClient.post(ApiPaths.Account.ChangeRole, request);
  }

  private paramsFromFilter(filter: UserFilter): HttpParams {
    let params = new HttpParams();

    if (filter.userName) {
      params = params.set('userName', filter.userName);
    }

    if (filter.role) {
      params = params.set('role', filter.role);
    }

    return params;
  }
}
