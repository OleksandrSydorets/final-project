﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CardFile.Identity.Migrations
{
    public partial class AddRolesOnMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "b7cc07ad-7314-446f-aebb-5d8b923adcb1", "fd8aea0e-b181-4934-bbc5-f9725ceb2b16", "User", "USER" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "fe66f269-60ef-4290-a967-caf89c3adeac", "a216f869-e5e4-471a-a5ea-f47dd8c88a56", "Admin", "ADMIN" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "b7cc07ad-7314-446f-aebb-5d8b923adcb1");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "fe66f269-60ef-4290-a967-caf89c3adeac");
        }
    }
}
