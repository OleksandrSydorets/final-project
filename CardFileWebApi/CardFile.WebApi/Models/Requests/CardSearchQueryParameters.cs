﻿namespace CardFile.WebApi.Models.Requests
{
    /// <summary>
    /// Represents query parameters for card search request
    /// </summary>
    public class CardSearchQueryParameters
    {
        public string Title { get; set; }
        public string AuthorId { get; set; }
        public string Author { get; set; }
        public bool IncludeUnverified { get; set; } = false;
        public DateTime? After { get; set; }
        public DateTime? Before { get; set; }
        public IEnumerable<string> Keywords { get; set; }
        public int Limit { get; set; } = 10;
        public int Offset { get; set; } = 0;

        /// <summary>
        /// Return <see langword="true"/> if no filtering properties beyond limit and offset are set.
        /// Otherwise returns <see langword="flase"/>.
        /// </summary>
        /// <returns></returns>
        public bool IsEmpty()
        {
            return string.IsNullOrEmpty(Title)
                && string.IsNullOrEmpty(AuthorId)
                && string.IsNullOrEmpty(Author)
                && !IncludeUnverified
                && After is null
                && Before is null
                && (Keywords is null || !Keywords.Any());
        }
    }
}
