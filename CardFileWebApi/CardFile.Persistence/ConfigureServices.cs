﻿using CardFile.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Persistence
{
    /// <summary>
    /// Contains extension method for adding <see cref="Persistence"/> services.
    /// </summary>
    public static class ConfigureServices
    {
        private const string ConnectionStringName = "ApplicationDB";

        /// <summary>
        /// Adds <see cref="Persistence"/> services.
        /// </summary>
        /// <param name="services">the <see cref="IServiceCollection"/> to add services to</param>
        /// <param name="configuration">configuration</param>
        /// <returns>A reference to this instance after the operation has completed.</returns>
        public static IServiceCollection AddCardFilePersistence(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<CardFileDbContext>(options =>
            {
                options.UseSqlServer(configuration.GetConnectionString(ConnectionStringName));
            });

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            return services;
        }
    }
}
