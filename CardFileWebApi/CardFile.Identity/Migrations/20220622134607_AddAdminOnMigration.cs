﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CardFile.Identity.Migrations
{
    public partial class AddAdminOnMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { "6f193093-c9e1-42f4-8b93-1bd154ca9c0b", 0, "8b5a93c4-9a6c-4859-8027-e7003ce6aede", null, false, true, null, null, "ADMIN", "AQAAAAEAACcQAAAAEBj2l1CS0A1TpZSvw6C8p7CHl56gIMzlsf4cy1UGNa4WjTbT8jZvqRKK+5FyqKWorQ==", null, false, "2EY6ECVKEALZD5JTNTB2XT663OMWMPDV", false, "Admin" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "fe66f269-60ef-4290-a967-caf89c3adeac", "6f193093-c9e1-42f4-8b93-1bd154ca9c0b" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "fe66f269-60ef-4290-a967-caf89c3adeac", "6f193093-c9e1-42f4-8b93-1bd154ca9c0b" });

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "6f193093-c9e1-42f4-8b93-1bd154ca9c0b");
        }
    }
}
