﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Identity.Entities
{
    /// <summary>
    /// Represent a role users of this application can have in identity system.
    /// </summary>
    public class CardFileRole : IdentityRole
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CardFileRole"/> class.
        /// </summary>
        public CardFileRole(): base() { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CardFileRole"/> class
        /// and sets it's Name property.
        /// </summary>
        public CardFileRole(string roleName) : base(roleName) { }

        /// <summary>
        /// Users that belong to this role.
        /// </summary>
        public ICollection<CardFileUser> Users { get; set; }
    }
}
