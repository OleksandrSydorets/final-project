﻿using CardFile.Domain.Entities;
using CardFile.Domain.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Persistence.Helpers
{
    /// <summary>
    /// Contains extension method for work with <see cref="CardSearchFilter"/>.
    /// </summary>
    public static class FilterExtensions
    {
        /// <summary>
        /// Applies <see cref="CardSearchFilter"/> to an <see cref="IQueryable{T}"/> of <see cref="Card"/>.
        /// </summary>
        /// <param name="query">card query</param>
        /// <param name="filter">filter</param>
        /// <returns>Queryable with filter applied.</returns>
        public static IQueryable<Card> ApplyFilter(this IQueryable<Card> query, CardSearchFilter filter)
        {
            var result = query;

            if (!string.IsNullOrWhiteSpace(filter.Title))
            {
                result = result.Where(c => c.Title.Contains(filter.Title));
            }

            if (!string.IsNullOrWhiteSpace(filter.AuthorId))
            {
                result = result.Where(c => c.AuthorId == filter.AuthorId);
            }

            if (!string.IsNullOrWhiteSpace(filter.Author))
            {
                result = result.Where(c => c.Author.DisplayName.Contains(filter.Author));
            }

            if (!filter.IncludeUnverified)
            {
                result = result.Where(c => c.Verified);
            }

            if (filter.After is not null)
            {
                result = result.Where(c => c.CreatedAt > filter.After);
            }

            if (filter.Before is not null)
            {
                result = result.Where(c => c.CreatedAt < filter.Before);
            }

            if (filter.Keywords is not null && filter.Keywords.Any())
            {
                foreach (string keyword in filter.Keywords)
                {
                    result = result.Where(c => c.Keywords.Any(k => k.Name == keyword));
                }
            }

            return result;
        }
    }
}
