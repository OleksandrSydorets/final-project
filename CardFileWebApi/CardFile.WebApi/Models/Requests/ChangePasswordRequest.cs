﻿using System.ComponentModel.DataAnnotations;

namespace CardFile.WebApi.Models.Requests
{
    public class ChangePasswordRequest
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string CurrentPassword { get; set; }

        [Required]
        public string NewPassword { get; set; }
    }
}
