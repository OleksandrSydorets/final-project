import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { ApiPaths } from '../api-paths';
import { Keyword } from '../models/keyword';

@Injectable({
  providedIn: 'root'
})
export class KeywordService {

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Keyword[]> {
    return this.httpClient.get<Keyword[]>(ApiPaths.Keywords.Base);
  }

  getNames(): Observable<string[]> {
    return this.getAll().pipe(map(keywords => keywords.map(keyword => keyword.name)));
  }

  get(id: number): Observable<Keyword> {
    return this.httpClient.get<Keyword>(ApiPaths.Keywords.byId(id))
  }

  post(name: string): Observable<Keyword> {
    const request = { name: name };
    return this.httpClient.post<Keyword>(ApiPaths.Keywords.Base, request);
  }

  put(id: number, name: string): Observable<Keyword> {
    const request = { name: name };
    return this.httpClient.put<Keyword>(ApiPaths.Keywords.byId(id), request);
  }

  delete(id: number) {
    return this.httpClient.delete(ApiPaths.Keywords.byId(id));
  }
}
