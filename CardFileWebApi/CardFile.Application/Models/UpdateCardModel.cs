﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Models
{
    /// <summary>
    /// Data transfer object that contains properties needed to update an index card.
    /// </summary>
    public class UpdateCardModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public IEnumerable<string> Keywords { get; set; }
    }
}
