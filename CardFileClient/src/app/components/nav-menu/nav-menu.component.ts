import { Component, OnInit } from '@angular/core';
import { UserRole } from 'src/app/models/user-role';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  constructor(public authService: AuthenticationService) { }

  ngOnInit(): void {
  }

  get showKeywords() {
    return UserRole.isModeratorOrAdmin(this.authService.role);
  }

  get showUsers() {
    return this.authService.role === UserRole.Admin;
  }
}
