﻿using CardFile.Domain;
using CardFile.Domain.Repositories;
using CardFile.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Persistence
{
    /// <summary>
    /// An <see cref="IUnitOfWork"/> implementation.
    /// </summary>
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CardFileDbContext _dbContext;
        private CardRepository _cardRepository;
        private KeywordRepository _keywordRepository;
        private UserRepository _userRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        /// <param name="dbContext">database context</param>
        /// <exception cref="ArgumentNullException">
        /// <paramref name="dbContext"/> is <see langword="null"/>.
        /// </exception>
        public UnitOfWork(CardFileDbContext dbContext)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        /// <inheritdoc/>
        public ICardRepository CardRepository => _cardRepository ??= new CardRepository(_dbContext);

        /// <inheritdoc/>
        public IKeywordRepository KeywordRepository => _keywordRepository ??= new KeywordRepository(_dbContext);

        /// <inheritdoc/>
        public IUserRepository UserRepository => _userRepository ??= new UserRepository(_dbContext);

        /// <inheritdoc/>
        public async Task SaveAsync()
        {
            await _dbContext.SaveChangesAsync();
        }
    }
}
