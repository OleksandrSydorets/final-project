export interface AuthFailedResponse {
    errors: string[]
}

export function isAuthFailedResponse(value: any): boolean {
    if (typeof value !== 'object') {
        return false;
    }

    if (!('errors' in value)) {
        return false;
    }

    if (!Array.isArray(value.errors)) {
        return false;
    }

    if (!value.errors.every((x: any) => typeof x === 'string')) {
        return false;
    }

    return true;
}