﻿using CardFile.Domain.Entities;
using CardFile.Domain.Filters;
using CardFile.Domain.Repositories;
using CardFile.Persistence.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Persistence.Repositories
{
    /// <summary>
    /// An <see cref="ICardRepository"/> implementation.
    /// </summary>
    public class CardRepository : ICardRepository
    {
        private readonly DbSet<Card> _cards;

        /// <summary>
        /// Initializes a new instance of the <see cref="CardRepository"/> class.
        /// </summary>
        /// <param name="dbContext">database context</param>
        /// <exception cref="ArgumentException">
        /// <paramref name="dbContext"/> is <see langword="null"/>
        /// </exception>
        public CardRepository(CardFileDbContext dbContext)
        {
            ArgumentNullException.ThrowIfNull(dbContext);

            _cards = dbContext.Cards;
        }

        /// <inheritdoc/>
        public Task<IEnumerable<Card>> GetAllAsync(bool includeUnverified = false, int limit = 10, int offset = 0)
        {
            if (limit <= 0)
            {
                throw new ArgumentException("limit can't be negative or zero", nameof(limit));
            }

            if (offset < 0)
            {
                throw new ArgumentException("offset can't be negative", nameof(offset));
            }

            return GetAllInternalAsync(includeUnverified);

            async Task<IEnumerable<Card>> GetAllInternalAsync(bool includeUnverified)
            {
                var query = includeUnverified ? _cards : _cards.Where(c => c.Verified);

                return await query
                    .Include(c => c.Author)
                    .Include(c => c.Keywords)
                    .OrderByDescending(c => c.CreatedAt)
                    .Skip(offset)
                    .Take(limit)
                    .ToListAsync();
            }
        }

        /// <inheritdoc/>
        public Task<IEnumerable<Card>> GetByFilterAsync(CardSearchFilter filter, int limit = 10, int offset = 0)
        {
            ArgumentNullException.ThrowIfNull(filter);

            if (limit <= 0)
            {
                throw new ArgumentException("limit can't be negative or zero", nameof(limit));
            }

            if (offset < 0)
            {
                throw new ArgumentException("offset can't be negative", nameof(offset));
            }

            return GetByFilterInternalAsync(filter, limit, offset);

            async Task<IEnumerable<Card>> GetByFilterInternalAsync(CardSearchFilter filter, int limit, int offset)
            {
                return await _cards
                    .Include(c => c.Author)
                    .Include(c => c.Keywords)
                    .ApplyFilter(filter)
                    .OrderByDescending(c => c.CreatedAt)
                    .Skip(offset)
                    .Take(limit)
                    .ToListAsync();
            }
        }

        /// <inheritdoc/>
        public async Task<Card> GetByIdAsync(int id)
        {
            return await _cards
                .Include(c => c.Author)
                .Include(c => c.Keywords)
                .FirstOrDefaultAsync(c => c.Id == id);
        }

        /// <inheritdoc/>
        public async Task<Card> GetByIdWithContentAsync(int id)
        {
            return await _cards
                .Include(c => c.Author)
                .Include(c => c.Keywords)
                .Include(c => c.Content)
                .AsSplitQuery()
                .FirstOrDefaultAsync(c => c.Id == id);
        }

        /// <inheritdoc/>
        public async Task AddAsync(Card card)
        {
            await _cards.AddAsync(card);
        }

        /// <inheritdoc/>
        public void Update(Card card)
        {
            _cards.Update(card);
        }

        /// <inheritdoc/>
        public void Delete(Card card)
        {
            _cards.Remove(card);
        }

        /// <inheritdoc/>
        public async Task DelelteByIdAsync(int id)
        {
            var card = await _cards.FindAsync(id);

            if (card is not null)
                _cards.Remove(card);
        }

        /// <inheritdoc/>
        public Task<int> Count(bool countUnverified)
        {
            var query = countUnverified ? _cards : _cards.Where(c => c.Verified);

            return query.CountAsync();
        }

        /// <inheritdoc/>
        public Task<int> CountFiltered(CardSearchFilter filter)
        {
            ArgumentNullException.ThrowIfNull(filter);

            return _cards
                .ApplyFilter(filter)
                .CountAsync();
        }
    }
}
