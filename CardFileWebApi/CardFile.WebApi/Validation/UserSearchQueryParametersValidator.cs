﻿using CardFile.Application.Models;
using CardFile.WebApi.Models.Requests;
using FluentValidation;

namespace CardFile.WebApi.Validation
{
    /// <summary>
    /// <see cref="FluentValidation"/> validator for <see cref="UserSearchQueryParameters"/>.
    /// </summary>
    public class UserSearchQueryParametersValidator : AbstractValidator<UserSearchQueryParameters>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserSearchQueryParametersValidator"/> class.
        /// </summary>
        public UserSearchQueryParametersValidator()
        {
            RuleFor(m => m.Role)
                .Must(BeNullOrValidRole).WithMessage("{PropertyValue} is not a valid role.");

            RuleFor(m => m.Limit)
                .GreaterThan(0);

            RuleFor(m => m.Offset)
                .GreaterThanOrEqualTo(0);
        }

        private static bool BeNullOrValidRole(string role)
        {
            if (string.IsNullOrEmpty(role))
            {
                return true;
            }

            return UserRoles.Roles.Contains(role);
        }
    }
}
