﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardFile.Application.Models
{
    /// <summary>
    /// Data transfer object that represents criteria by which users can be filtered.
    /// </summary>
    public class UserSearchFilterModel
    {
        public string UserName { get; set; }
        public string Role { get; set; }
    }
}
