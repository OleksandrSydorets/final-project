import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { AuthenticationService } from '../../services/authentication.service';
import { ControlValidators } from '../../control-validators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  errors: string[] | null = null;
  registrationForm!: FormGroup;

  constructor(private authService: AuthenticationService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.registrationForm = this.formBuilder.group({
      userName: ['', Validators.required],
      passwords: this.formBuilder.group({
        password: ['', [
          Validators.required,
          Validators.minLength(6),
          ControlValidators.hasDigit,
          ControlValidators.haslowerCaseLetter,
          ControlValidators.hasUpperCaseLetter,
          ControlValidators.hasNonAlphaNumeric,
        ]],
        confirmPassword: ['']
      }, { validators: ControlValidators.passwordsMatch })
    });
  }

  onSubmit() {
    if (this.registrationForm.invalid) {
      this.registrationForm.markAllAsTouched();
      return;
    }

    this.registrationForm.disable();
    this.errors = null;

    const userName: string = this.registrationForm.get('userName')!.value;
    const password: string = this.registrationForm.get('passwords.password')!.value;

    this.authService.register(userName, password)
      .subscribe(result => {
        if (result.successful) {
          this.router.navigate(['']);
        } else {
          this.errors = result.errors!;
        }

        this.registrationForm.enable();
      })
  }

  controlValidCss(controlName: string) {
    const control = this.registrationForm.get(controlName);

    if (!control) {
      return { };
    }

    return {
      'is-invalid': control.invalid && control.touched,
    }
  }

  confirmPasswordValidCss() {
    const group = this.registrationForm.get('passwords')

    return {
      'is-invalid': group?.touched && group?.hasError('passwordsMatch')
    };
  }
}
