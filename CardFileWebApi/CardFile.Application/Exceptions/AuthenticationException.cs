﻿using System.Runtime.Serialization;

namespace CardFile.Application.Exceptions
{
    /// <summary>
    /// The exception that is thrown when error occurs during authentication, registration or password change.
    /// </summary>
    [Serializable]
    public class AuthenticationException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationException"/> class with error strings.
        /// </summary>
        /// <param name="errors">errors</param>
        public AuthenticationException(IEnumerable<string> errors)
        {
            Errors = errors is not null ? errors : Enumerable.Empty<string>();
        }

        /// <inheritdoc/>
        protected AuthenticationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            Errors = (IEnumerable<string>)info.GetValue(nameof(Errors), typeof(IEnumerable<string>));
        }

        /// <summary>
        /// Gets error strings.
        /// </summary>
        public IEnumerable<string> Errors { get; }

        /// <inheritdoc/>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(nameof(Errors), Errors);

            base.GetObjectData(info, context);
        }
    }
}
